package com.noobs2d.jumbocinema;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @author ThePreviousDev, MrUseL3tter
 */
public final class MySQLDatabase {

    private static MySQLDatabase instance;
    private static final String LOCALHOST_DATABASE = "jumbocinema_com";
    private static final String LOCALHOST_DB_IP = "localhost";
    private static final String LOCALHOST_PASSWORD = "";
    private static final String LOCALHOST_PORT = "3306";
    private static final String LOCALHOST_USER = "root";
    private static final String SERVER_DATABASE = "jumbocinema_com";
    private static final String SERVER_DB_IP = "172.16.17.5";
    private static final String SERVER_PASSWORD = "EeSaGheej1da";
    private static final String SERVER_PORT = "3306";

    private static final String SERVER_USER = "jumbocinema_com";

    private Connection connection;
    private Statement statement;
    static {
	instance = new MySQLDatabase();
    }

    public static void connect() throws SQLException, ClassNotFoundException {
	Class.forName("com.mysql.jdbc.Driver");
	instance.connection = DriverManager.getConnection(MySQLDatabase.getFullURL());
	instance.statement = instance.connection.createStatement();
    }

    public static void disconnect() throws SQLException {
	instance.connection.close();
    }

    public static ResultSet execute(String query) throws SQLException, ClassNotFoundException {
	instance.statement.execute(query);
	ResultSet resultSet = instance.statement.getResultSet();
	return resultSet;
    }

    public static ResultSet executeQuery(String query) throws SQLException, ClassNotFoundException {
	ResultSet resultSet = instance.statement.executeQuery(query);
	return resultSet;
    }

    public static int executeUpdate(String updateStatement) throws SQLException, ClassNotFoundException {
	int result = instance.statement.executeUpdate(updateStatement);
	return result;
    }

    /** @returns {@link Statement#getGeneratedKeys()} */
    public static ResultSet executeUpdate(String updateStatement, int autoGeneratedKeys) throws SQLException, ClassNotFoundException {
	instance.statement.executeUpdate(updateStatement, autoGeneratedKeys);
	return instance.statement.getGeneratedKeys();
    }

    public static String getFullURL() {
	if (Main.IS_PRODUCTION)
	    return "jdbc:mysql://" + MySQLDatabase.getHost() + ":3306/" + MySQLDatabase.getName() + "?user=" + MySQLDatabase.getUsername() + "&password=" + MySQLDatabase.getPassword();
	else
	    return "jdbc:mysql://" + MySQLDatabase.getHost() + ":" + getPort() + "/" + MySQLDatabase.getName() + "?user=" + MySQLDatabase.getUsername() + "&password=" + MySQLDatabase.getPassword();

    }

    public static String getHost() {
	if (Main.IS_PRODUCTION)
	    return SERVER_DB_IP;
	else
	    return LOCALHOST_DB_IP;
    }

    public static String getName() {
	if (Main.IS_PRODUCTION)
	    return SERVER_DATABASE;
	else
	    return LOCALHOST_DATABASE;
    }

    public static String getPassword() {
	if (Main.IS_PRODUCTION)
	    return SERVER_PASSWORD;
	else
	    return LOCALHOST_PASSWORD;
    }

    public static String getPort() {
	if (Main.IS_PRODUCTION)
	    return SERVER_PORT;
	else
	    return LOCALHOST_PORT;
    }

    public static String getUsername() {
	if (Main.IS_PRODUCTION)
	    return SERVER_USER;
	else
	    return LOCALHOST_USER;
    }

    /** Singleton, motherfuckers! */
    protected MySQLDatabase() {
    }

}
