package com.noobs2d.jumbocinema.feedback;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.noobs2d.jumbocinema.ExtensionRequestHandler;
import com.noobs2d.jumbocinema.Main;
import com.smartfoxserver.v2.SmartFoxServer;
import com.smartfoxserver.v2.config.ServerSettings.MailerSettings;
import com.smartfoxserver.v2.entities.Email;
import com.smartfoxserver.v2.entities.SFSEmail;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.entities.managers.IMailerService;

public class FeedbackMailer extends ExtensionRequestHandler {

    public static String TAG = "FEEDBACK_MAIL";

    private static String FROM_MAIL_ADDRESS = "jumbocinema@hotmail.com";
    private static String FROM_MAIL_PASSWORD = "JumboCine99";
    private static String MAIL_SUBJECT = "Feedback for Jumbo Cinema";
    private static String TO_MAIL_ADDRESS = "support@jumbocinema.com";

    @Override
    public void handleClientRequest(User sender, ISFSObject params) {

	Properties props = new Properties();
	props.put("mail.smtp.host", "smtp.gmail.com"); //SMTP Host
	props.put("mail.smtp.port", "587"); //TLS Port
	props.put("mail.smtp.auth", "true"); //enable authentication
	props.put("mail.smtp.starttls.enable", "true"); //enable STARTTLS

	//create Authenticator object to pass in Session.getInstance argument
	Authenticator auth = new Authenticator() {

	    //override the getPasswordAuthentication method
	    @Override
	    protected PasswordAuthentication getPasswordAuthentication() {
		return new PasswordAuthentication(FROM_MAIL_ADDRESS, FROM_MAIL_PASSWORD);
	    }
	};
	Session session = Session.getInstance(props, auth);
	session.setDebug(true);

	String userMail = params.getUtfString("USER_MAIL");
	String message = params.getUtfString("MESSAGE") + "\n\n" + userMail;

	sendEmailViaSFS(sender, params);
	//	sendEmail(sender, session, message);
    }

    public void sendEmail(User sender, Session session, String body) {
	ISFSObject returnParams = SFSObject.newInstance();
	try {
	    MimeMessage msg = new MimeMessage(session);
	    //set message headers
	    msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
	    msg.addHeader("format", "flowed");
	    msg.addHeader("Content-Transfer-Encoding", "8bit");

	    msg.setFrom(new InternetAddress(FROM_MAIL_ADDRESS, MAIL_SUBJECT));

	    msg.setReplyTo(InternetAddress.parse(FROM_MAIL_ADDRESS, false));

	    msg.setSubject(MAIL_SUBJECT, "UTF-8");

	    msg.setText(body, "UTF-8");

	    msg.setSentDate(new Date());

	    trace("Sending email to " + TO_MAIL_ADDRESS + "...");
	    msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(TO_MAIL_ADDRESS, false));
	    Transport.send(msg);

	    returnParams.putInt("FLAGS", Main.SUCCESS);
	    returnParams.putUtfString("FLAG_FEED", "Feedback successfully sent!");
	} catch (MessagingException e) {
	    e.printStackTrace();
	    trace("Email sending error: " + e.getMessage());
	    returnParams.putInt("FLAGS", Main.FAILED);
	    returnParams.putUtfString("FLAG_FEED", "There was an error trying to send your feedback. Please try again.");
	} catch (UnsupportedEncodingException e) {
	    e.printStackTrace();
	    trace("Email sending error: " + e.getMessage());
	    returnParams.putInt("FLAGS", Main.FAILED);
	    returnParams.putUtfString("FLAG_FEED", "There was an error trying to send your feedback. Please try again.");
	} catch (Exception e) {
	    e.printStackTrace();
	    trace("Email sending error: " + e.getMessage());
	    returnParams.putInt("FLAGS", Main.FAILED);
	    returnParams.putUtfString("FLAG_FEED", "There was an error trying to send your feedback. Please try again.");
	}

	//	    e.printStackTrace();
	//	    trace("Email sending error: " + e.getMessage());
	//	    returnParams.putInt("FLAGS", Main.FAILED);
	//	    returnParams.putUtfString("FLAG_FEED", "There was an error trying to send your feedback. Please try again.");
	send(TAG, returnParams, sender);
    }

    private void sendEmailViaSFS(User sender, ISFSObject params) {
	ISFSObject returnParams = SFSObject.newInstance();
	String userMail = params.getUtfString("USER_MAIL");
	String message = params.getUtfString("MESSAGE") + "\n\n" + userMail;
	Email myEmail = new SFSEmail(FROM_MAIL_ADDRESS, TO_MAIL_ADDRESS, MAIL_SUBJECT, message);
	try {
	    IMailerService mailerService = SmartFoxServer.getInstance().getMailService();
	    MailerSettings mailerSettings = mailerService.getConfiguration();
	    trace("Mailer host: " + mailerSettings.mailHost);
	    trace("Mailer user: " + mailerSettings.mailUser);
	    trace("Mailer password: " + mailerSettings.mailPass);
	    trace("SMTP port: " + mailerSettings.smtpPort);
	    returnParams.putInt("FLAGS", Main.SUCCESS);
	    mailerService.sendMail(myEmail);
	    returnParams.putUtfString("FLAG_FEED", "Feedback successfully sent!");
	} catch (MessagingException e) {
	    e.printStackTrace();
	    trace(e);
	    returnParams.putInt("FLAGS", Main.FAILED);
	    returnParams.putUtfString("FLAG_FEED", "Error sending email: " + e.getMessage());
	} catch (Exception e) {
	    e.printStackTrace();
	    trace(e);
	    returnParams.putInt("FLAGS", Main.FAILED);
	    returnParams.putUtfString("FLAG_FEED", "Error sending email: " + e.getMessage());
	}
	send(TAG, returnParams, sender);
    }
}
