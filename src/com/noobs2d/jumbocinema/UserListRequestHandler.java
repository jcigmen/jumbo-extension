package com.noobs2d.jumbocinema;

import java.util.ArrayList;
import java.util.Iterator;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;

public class UserListRequestHandler extends ExtensionRequestHandler {

    public static final String TAG = "USER_LIST";

    @Override
    public void handleClientRequest(User sender, ISFSObject params) {
	Iterator<User> iterator = sender.getZone().getUserList().iterator();
	ArrayList<String> names = new ArrayList<String>();
	ArrayList<String> ids = new ArrayList<String>();
	int index = 0;
	while (iterator.hasNext()) {
	    User user = iterator.next();
	    names.add(user.getName());
	    ids.add(user.getVariable("uid").getStringValue());
	    index++;
	}
	ISFSObject returnParams = SFSObject.newInstance();
	returnParams.putUtfStringArray("USER_IDS", ids);
	returnParams.putUtfStringArray("USERNAMES", names);
	send(TAG, returnParams, sender);
    }

}
