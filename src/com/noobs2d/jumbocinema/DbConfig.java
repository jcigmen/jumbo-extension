package com.noobs2d.jumbocinema;

public class DbConfig {

    public static final String LOCALHOST_DATABASE = "jumbocinema_com"/* "jumbotest" */;
    public static final String LOCALHOST_DB_IP = "localhost"/* "localhost" */;
    public static final String LOCALHOST_PASSWORD = ""/* "thuglife" */;
    public static final String LOCALHOST_USER = "root"/* "michael" */;

    public static final String SERVER_DATABASE = "jumbocinema_com";
    public static final String SERVER_DB_IP = "172.16.17.5";
    public static final String SERVER_PASSWORD = "EeSaGheej1da";
    public static final String SERVER_USER = "jumbocinema_com";

    public static String getDb() {
	if (Main.IS_PRODUCTION)
	    return SERVER_DATABASE;
	else
	    return LOCALHOST_DATABASE;
    }

    public static String getDbIp() {
	if (Main.IS_PRODUCTION)
	    return SERVER_DB_IP;
	else
	    return LOCALHOST_DB_IP;
    }

    public static String getPassword() {
	if (Main.IS_PRODUCTION)
	    return SERVER_PASSWORD;
	else
	    return LOCALHOST_PASSWORD;
    }

    public static String getUser() {
	if (Main.IS_PRODUCTION)
	    return SERVER_USER;
	else
	    return LOCALHOST_USER;
    }

}
