package com.noobs2d.jumbocinema;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import org.json.JSONArray;
import org.json.JSONException;

import com.noobs2d.jumbocinema.classes.Functions;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

public class GetShowtimeSchedule extends BaseClientRequestHandler
{
	public void handleClientRequest(User sender, ISFSObject params)
	{
		String dbUrl = "jdbc:mysql://" + DbConfig.getDbIp() + "/" + DbConfig.getDb() + "?user=" + DbConfig.getUser() + "&password=" + DbConfig.getPassword();/*"jdbc:mysql://localhost/jumbotest?user=michael&password=thuglife";*/
		
		try 
		{
			Class.forName("com.mysql.jdbc.Driver");
		
			Connection con = DriverManager.getConnection(dbUrl);
			Statement stmt = con.createStatement();
			
			JSONArray jsonArr	= Functions.getJsonMovieSchedule(stmt);
			
			if (jsonArr != null)
			{
				ISFSObject resObj = SFSObject.newInstance();        
		        resObj.putUtfString("showtimeJson", jsonArr.toString());
		         
		        // Send it back
		        send("getShowtimeSchedule", resObj, sender);		
			}		
		} 
		catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
}
