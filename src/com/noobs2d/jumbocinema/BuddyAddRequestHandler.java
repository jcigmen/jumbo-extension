package com.noobs2d.jumbocinema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;

public class BuddyAddRequestHandler extends ExtensionRequestHandler {

    public static final String TAG = "BUDDY_ADD";

    @Override
    public void handleClientRequest(User sender, ISFSObject param) {
	final ISFSObject returnParams = SFSObject.newInstance();
	final String userID = param.getUtfString("USER_ID1");
	final String userToAddID = param.getUtfString("USER_ID2");
	trace("[BuddyAddRequestHandler] " + userID + " is going to add " + userToAddID);
	final String addBuddyQuery = "CALL createBuddyRelation('" + userID + "', '" + userToAddID + "');";
	try {
	    MySQLDatabase.connect();

	    // remove the block relation if there is one
	    String query = "CALL deleteBlockRelation(" + userID + ", " + userToAddID + ");";
	    MySQLDatabase.executeQuery(query);

	    // if this query yields a row, they are already friends
	    final String selectBuddyQuery = "CALL selectBuddyByUserID('" + userID + "', '" + userToAddID + "');";
	    ResultSet resultSet = MySQLDatabase.executeQuery(selectBuddyQuery);
	    boolean alreadyFriends = resultSet != null && resultSet.next();

	    if (alreadyFriends) {
		returnParams.putInt("FLAGS", Main.FAILED);
		returnParams.putUtfString("FLAG_FEED", "You are already friends with this user!");
	    } else {
		MySQLDatabase.executeUpdate(addBuddyQuery);
		returnParams.putInt("FLAGS", Main.SUCCESS);
		returnParams.putUtfString("FLAG_FEED", "You are now friends with " + param.getUtfString("USER_NAME") + ".");
	    }
	    MySQLDatabase.disconnect();
	} catch (SQLException e) {
	    onExceptionEvent("BuddyAddRequestHandler", e, returnParams);
	} catch (ClassNotFoundException e) {
	    onExceptionEvent("BuddyAddRequestHandler", e, returnParams);
	}
	send(TAG, returnParams, sender);
    }
}
