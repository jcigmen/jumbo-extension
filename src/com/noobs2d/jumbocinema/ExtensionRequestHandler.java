package com.noobs2d.jumbocinema;

import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

public abstract class ExtensionRequestHandler extends BaseClientRequestHandler {

    protected void attachFailedFeedToResponse(ISFSObject response, String message) {
	response.putInt("FLAGS", Main.FAILED);
	response.putUtfString("FLAG_FEED", message);
    }

    protected void attachSuccessFeedToResponse(ISFSObject response, String message) {
	response.putInt("FLAGS", Main.SUCCESS);
	response.putUtfString("FLAG_FEED", message);
    }

    protected void onExceptionEvent(String callingClass, Exception e, ISFSObject returnParams) {
	if (returnParams.containsKey("FLAGS"))
	    returnParams.removeElement("FLAGS");
	if (returnParams.containsKey("FLAG_FEED"))
	    returnParams.removeElement("FLAG_FEED");
	if (returnParams.containsKey("FLAG_ERROR"))
	    returnParams.removeElement("FLAG_ERROR");
	returnParams.putInt("FLAGS", Main.ERROR);
	returnParams.putUtfString("FLAG_ERROR", e.toString());
	trace("[" + callingClass + "] Error: " + e);
	e.printStackTrace();
    }
}
