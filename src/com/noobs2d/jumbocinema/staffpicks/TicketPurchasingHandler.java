package com.noobs2d.jumbocinema.staffpicks;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.noobs2d.jumbocinema.ExtensionRequestHandler;
import com.noobs2d.jumbocinema.Main;
import com.noobs2d.jumbocinema.MySQLDatabase;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;

/**
 * @author MrUseL3tter, michael
 */
public class TicketPurchasingHandler extends ExtensionRequestHandler {

    public static final String TAG = "STAFF_PICK_PURCHASE";

    @Override
    public void handleClientRequest(User sender, ISFSObject params) {
	final ISFSObject returnParams = SFSObject.newInstance();
	String userID = params.getUtfString("USER_ID");
	String showtimeID = params.getUtfString("SHOWTIME_ID");
	try {
	    MySQLDatabase.connect();

	    // Create a new purchase record
	    String purchaseQuery = "CALL staffPicksPurchaseTicket(" + userID + ", " + showtimeID + ");";
	    ResultSet resultSet = MySQLDatabase.executeQuery(purchaseQuery);

	    int count = StaffPicksUtil.putStandardScheduleDataIntoSFSObject(resultSet, returnParams);
	    if (count >= 0) {
		returnParams.putInt("FLAGS", Main.SUCCESS);
		returnParams.putUtfString("FLAG_FEED", "Successfully purchased a ticket!");
	    } else {
		returnParams.putInt("FLAGS", Main.FAILED);
		returnParams.putUtfString("FLAG_FEED", "Failed purchasing a ticket. Please try again.");
	    }

	    MySQLDatabase.disconnect();
	} catch (SQLException e) {
	    onExceptionEvent(getClass().getName(), e, returnParams);
	} catch (ClassNotFoundException e) {
	    onExceptionEvent(getClass().getName(), e, returnParams);
	}
	send(TAG, returnParams, sender);
    }
}
