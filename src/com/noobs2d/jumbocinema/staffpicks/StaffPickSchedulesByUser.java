package com.noobs2d.jumbocinema.staffpicks;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.noobs2d.jumbocinema.ExtensionRequestHandler;
import com.noobs2d.jumbocinema.Main;
import com.noobs2d.jumbocinema.MySQLDatabase;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;

public class StaffPickSchedulesByUser extends ExtensionRequestHandler {

    public static final String TAG = "STAFF_PICKS_BY_USER";

    @Override
    public void handleClientRequest(User sender, ISFSObject params) {
	ISFSObject returnParams = SFSObject.newInstance();
	String userID = params.getUtfString("USER_ID");
	try {
	    MySQLDatabase.connect();

	    String query = "CALL staffPicksGetSchedulesByUserID('" + userID + "');";
	    ResultSet resultSet = MySQLDatabase.executeQuery(query);

	    int index = StaffPicksUtil.putStandardScheduleDataIntoSFSObject(resultSet, returnParams);
	    MySQLDatabase.disconnect();

	    if (index < 0) {
		returnParams.putInt("FLAGS", Main.FAILED);
		returnParams.putUtfString("FLAG_FEED", "There were no schedules fetched. You have no active Staff Pick schedules!");
	    } else {
		returnParams.putInt("ROWS", index + 1);
		returnParams.putInt("FLAGS", Main.SUCCESS);
		returnParams.putUtfString("FLAG_FEED", "Successfully retrieved Staff Picks schedules!");
	    }
	} catch (SQLException e) {
	    onExceptionEvent(getClass().getName(), e, returnParams);
	} catch (ClassNotFoundException e) {
	    onExceptionEvent(getClass().getName(), e, returnParams);
	}
	send(TAG, returnParams, sender);
    }
}
