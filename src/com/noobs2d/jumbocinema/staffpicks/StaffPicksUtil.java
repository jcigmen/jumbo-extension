package com.noobs2d.jumbocinema.staffpicks;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.smartfoxserver.v2.entities.data.ISFSObject;

public class StaffPicksUtil {

    public static int putStandardScheduleDataIntoSFSObject(ResultSet resultSet, ISFSObject returnParams) throws SQLException {
	int index = -1;
	while (resultSet.next()) {
	    index++;

	    String showtimeID = resultSet.getString("SHOWTIME_ID");
	    int elapsed = resultSet.getInt("ELAPSED");
	    int movieID = resultSet.getInt("MOVIE_ID");
	    String movieTitle = resultSet.getString("TITLE");
	    String genre = resultSet.getString("GENRE");
	    String director = resultSet.getString("DIRECTOR_FIRST_NAME") + " " + resultSet.getString("DIRECTOR_LAST_NAME");
	    String movieDescription = resultSet.getString("DESCRIPTION");
	    int movieDurationInSeconds = resultSet.getInt("DURATION");
	    String imageLink = resultSet.getString("IMAGE_LINK");
	    String youTubeLink = resultSet.getString("YOUTUBE_LINK");
	    int theaterNumber = resultSet.getInt("THEATER_NUMBER");

	    returnParams.putUtfString("SHOWTIME_ID[" + index + "]", showtimeID);
	    returnParams.putInt("ELAPSED[" + index + "]", elapsed);
	    returnParams.putInt("MOVIE_ID[" + index + "]", movieID);
	    returnParams.putUtfString("TITLE[" + index + "]", movieTitle);
	    returnParams.putUtfString("GENRE[" + index + "]", genre);
	    returnParams.putUtfString("DIRECTOR[" + index + "]", director);
	    returnParams.putUtfString("DESCRIPTION[" + index + "]", movieDescription);
	    returnParams.putInt("DURATION[" + index + "]", movieDurationInSeconds);
	    returnParams.putUtfString("IMAGE_LINK[" + index + "]", imageLink);
	    returnParams.putUtfString("YOUTUBE_LINK[" + index + "]", youTubeLink);
	    returnParams.putInt("THEATER_NUMBER[" + index + "]", theaterNumber);
	}

	return index;
    }
}
