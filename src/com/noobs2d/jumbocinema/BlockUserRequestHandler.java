/**
 * @author MrUseL3tter
 * @version 1.00
 */
package com.noobs2d.jumbocinema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

public class BlockUserRequestHandler extends BaseClientRequestHandler {

    public static final String TAG = "BLOCK_USER";

    @Override
    public void handleClientRequest(User sender, ISFSObject params) {
	final ISFSObject returnParams = SFSObject.newInstance();
	final String userID = params.getUtfString("USER_ID1");
	final String userIDToBlock = params.getUtfString("USER_ID2");

	try {
	    MySQLDatabase.connect();

	    String query = "CALL selectBlockedListByUserID(" + userID + ", " + userIDToBlock + ");";
	    ResultSet resultSet = MySQLDatabase.execute(query);
	    boolean notYetBlocked = !resultSet.next();

	    // Remove user from friendlist
	    String deleteBuddyRelationQuery = "CALL deleteBuddyRelation(" + userID + ", " + userIDToBlock + ");";
	    MySQLDatabase.executeQuery(deleteBuddyRelationQuery);

	    if (notYetBlocked) {
		trace("[UserBlockRequestHandler] Adding " + userIDToBlock + " to " + userID + "\'s blocklist...");
		query = "CALL createUserBlockRelation(" + userID + ", " + userIDToBlock + ");";
		MySQLDatabase.executeQuery(query);
		returnParams.putInt("FLAGS", Main.SUCCESS);
		returnParams.putUtfString("FLAG_FEED", params.getUtfString("USERNAME") + " is now on your blocklist.");
	    } else {
		trace("[UserBlockRequestHandler] " + userID + " is already blocked to " + userIDToBlock + " or vice versa.");
		returnParams.putInt("FLAGS", Main.FAILED);
		returnParams.putUtfString("FLAG_FEED", params.getUtfString("USERNAME") + " is already on your blocklist!");
	    }

	    MySQLDatabase.disconnect();
	} catch (SQLException e) {
	    onExceptionEvent(e, returnParams);
	} catch (ClassNotFoundException e) {
	    onExceptionEvent(e, returnParams);
	}
	send(TAG, returnParams, sender);
    }

    private void onExceptionEvent(Exception e, ISFSObject returnParams) {
	if (returnParams.containsKey("FLAGS"))
	    returnParams.removeElement("FLAGS");
	if (returnParams.containsKey("FLAG_FEED"))
	    returnParams.removeElement("FLAG_FEED");
	if (returnParams.containsKey("FLAG_ERROR"))
	    returnParams.removeElement("FLAG_ERROR");
	returnParams.putInt("FLAGS", Main.ERROR);
	returnParams.putUtfString("FLAG_FEED", "There was an error adding this user to your blocklist. Please try again later.");
	returnParams.putUtfString("FLAG_ERROR", e.toString());
	trace("[UserBlockRequestHandler] Error: " + e);
	e.printStackTrace();
    }
}
