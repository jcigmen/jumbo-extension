package com.noobs2d.jumbocinema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;

public class BlockRemoveRequestHandler extends ExtensionRequestHandler {

    public static final String TAG = "BLOCK_REMOVE";

    @Override
    public void handleClientRequest(User sender, ISFSObject params) {
	final ISFSObject returnParams = SFSObject.newInstance();
	final String userID = params.getUtfString("USER_ID1");
	final String userIDToUnblock = params.getUtfString("USER_ID2");
	final String usernameToUnfriend = params.getUtfString("USERNAME");

	try {
	    MySQLDatabase.connect();

	    // if this query yields a row, they are blocking each other
	    String query = "CALL selectBlockedListByUserID(" + userID + ", " + userIDToUnblock + ");";
	    ResultSet resultSet = MySQLDatabase.execute(query);
	    boolean blocksThisUser = resultSet.next();

	    if (blocksThisUser) {
		query = "CALL deleteBlockRelation(" + userID + ", " + userIDToUnblock + ");";
		MySQLDatabase.executeQuery(query);
		returnParams.putInt("FLAGS", Main.SUCCESS);
		returnParams.putUtfString("FLAG_FEED", "You have unblocked " + usernameToUnfriend + ".");
	    } else {
		returnParams.putInt("FLAGS", Main.FAILED);
		returnParams.putUtfString("FLAG_FEED", usernameToUnfriend + " is not on your blocklist.");
	    }

	    MySQLDatabase.disconnect();
	} catch (SQLException e) {
	    onExceptionEvent("BlockRemoveRequestHandler", e, returnParams);
	} catch (ClassNotFoundException e) {
	    onExceptionEvent("BlockRemoveRequestHandler", e, returnParams);
	}
	send(TAG, returnParams, sender);
    }
}
