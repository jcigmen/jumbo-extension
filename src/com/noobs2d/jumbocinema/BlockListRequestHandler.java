package com.noobs2d.jumbocinema;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSArray;
import com.smartfoxserver.v2.entities.data.SFSObject;

public class BlockListRequestHandler extends ExtensionRequestHandler {

    public static final String TAG = "BLOCK_ENLIST";

    @Override
    public void handleClientRequest(User sender, ISFSObject params) {
	final ISFSObject returnParams = SFSObject.newInstance();
	final String userID = params.getUtfString("USER_ID");
	final SFSArray usersYouBlockedNames = new SFSArray();
	final SFSArray usersWhoBlockedYouNames = new SFSArray();
	final ArrayList<Long> usersYouBlockedIDs = new ArrayList<Long>();
	final ArrayList<Long> usersWhoBlockedYouIDs = new ArrayList<Long>();

	try {
	    MySQLDatabase.connect();

	    String query = "CALL getUsersWhoBlockedYou(" + userID + ");";
	    ResultSet resultSet = MySQLDatabase.executeQuery(query);
	    while (resultSet.next()) {
		Long id = resultSet.getLong("id");
		String name = resultSet.getString("first_name") + " " + resultSet.getString("last_name");
		usersWhoBlockedYouNames.addUtfString(name);
		usersWhoBlockedYouIDs.add(id);
	    }

	    query = "CALL getUsersYouBlocked(" + userID + ");";
	    resultSet = MySQLDatabase.executeQuery(query);
	    while (resultSet.next()) {
		Long id = resultSet.getLong("id");
		String name = resultSet.getString("first_name") + " " + resultSet.getString("last_name");
		usersYouBlockedNames.addUtfString(name);
		usersYouBlockedIDs.add(id);
	    }

	    MySQLDatabase.disconnect();
	} catch (SQLException e) {
	    onExceptionEvent("BlockListRequestHandler", e, returnParams);
	} catch (ClassNotFoundException e) {
	    onExceptionEvent("BlockListRequestHandler", e, returnParams);
	}
	returnParams.putSFSArray("YOU_BLOCKED_LIST_NAMES", usersYouBlockedNames);
	returnParams.putLongArray("YOU_BLOCKED_LIST_IDS", usersYouBlockedIDs);
	returnParams.putSFSArray("BLOCKED_YOU_LIST_NAMES", usersWhoBlockedYouNames);
	returnParams.putLongArray("BLOCKED_YOU_LIST_IDS", usersWhoBlockedYouIDs);
	send(TAG, returnParams, sender);
    }
}
