package com.noobs2d.jumbocinema;

import com.noobs2d.jumbocinema.youtube.ScheduleTimer;
import com.smartfoxserver.openspace.OpenSpaceExtension;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;

public class MainZoneExtension extends OpenSpaceExtension {

    private ScheduleTimer timer;

    @Override
    public void _init() {
	super._init();
	trace("ZONE EXTENSION LOADED!");
	timer = new ScheduleTimer(this);
    }

    public ScheduleTimer getTimer() {
	return timer;
    }

    @Override
    public void handleClientRequest(String requestId, User sender, ISFSObject params) {
	// TODO Auto-generated method stub
	super.handleClientRequest(requestId, sender, params);
    }

}
