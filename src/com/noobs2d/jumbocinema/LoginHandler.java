package com.noobs2d.jumbocinema;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;

import com.noobs2d.jumbocinema.classes.Functions;
import com.noobs2d.jumbocinema.classes.Utils;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

/**
 * When a user logs in, if they haven't logged in before, they will be added to the database,
 * otherwise we'll just log their login time.
 * 
 * @author michael
 */
public class LoginHandler extends BaseClientRequestHandler {

    @Override
    public void handleClientRequest(User sender, ISFSObject params) {
	long sessionId = -1;
	long userId = -1;
	int locationId = -1;

	final String userID = params.getUtfString("USER_ID");
	String firstName = params.getUtfString("firstName");
	String lastName = params.getUtfString("lastName");
	String birthday = params.getUtfString("birthday");
	String link = params.getUtfString("link");
	String locale = params.getUtfString("locale");
	float timeZone = params.getFloat("timeZone");

	// Ticket info to return
	DateTime showtime = null;
	int showtimeId = -1;
	int theaterNo = -1;
	int durationMin = -1;
	long secondsElapsed = 0;
	String movieTitle = "";

	String desc = "";
	String imageLink = "";
	String movieLink = "";
	String movieDesc = "";
	String fullDate = "";

	int movieId = -1;

	// JSON representing showtimes
	JSONArray jsonArr = null;
	List<String> ad_list = null;

	String dbUrl = "jdbc:mysql://" + DbConfig.getDbIp() + "/" + DbConfig.getDb() + "?user=" + DbConfig.getUser() + "&password=" + DbConfig.getPassword();/*
																			      * "jdbc:mysql://localhost/jumbotest?user=michael&password=thuglife"
																			      * ;
																			      */

	String error = "";
	try {
	    Class.forName("com.mysql.jdbc.Driver");
	    Connection con = DriverManager.getConnection(dbUrl);
	    Statement stmt = con.createStatement();

	    /** Check to see if the user already exists, if not, create a new record **/
	    String query = "SELECT idusers FROM users WHERE idusers = '" + userID + "' LIMIT 1";
	    ResultSet rs = stmt.executeQuery(query);
	    if (rs != null && rs.next())
		; // trace("[LoginHandler] User already exists in the DB!");
	    else {
		// Now we can insert a new user
		query = "INSERT INTO users (idusers, firstName, lastName, birthday) VALUES ('" + userID + "', '" + firstName + "','" + lastName + "','" + birthday + "')";
		stmt.executeUpdate(query);
	    }

	    /** Insert record into the log table **/
	    query = "INSERT INTO log(userId, startTime, endTime) VALUES ('" + userID + "', NOW(), '0000-00-00 00:00:00')";
	    stmt.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);

	    rs = stmt.getGeneratedKeys();
	    if (rs != null && rs.next())
		sessionId = rs.getLong(1);

	    /** Check to see if the user has any current purchases **/
	    query = "CALL getPurchasesByUserID(" + userID + ");";
	    stmt.execute(query);

	    rs = stmt.getResultSet();
	    ArrayList<Integer> old_purchases = new ArrayList<Integer>();

	    if (rs != null)
		// Loop through the purchases
		// NB: will have to modify this logic to handle multiple tickets
		while (rs.next()) {
		    int purchase_id = rs.getInt("id");
		    durationMin = rs.getInt("duration");
		    theaterNo = rs.getInt("theatre");
		    showtimeId = rs.getInt("showtime_id");

		    imageLink = rs.getString("imageLink");
		    movieLink = rs.getString("youtube");
		    movieDesc = rs.getString("description");

		    movieTitle = rs.getString("title");
		    movieId = rs.getInt("movieId");

		    // Date handling code
		    showtime = new DateTime(rs.getTimestamp("date"));
		    String hour = showtime.getHourOfDay() < 10 ? "0" + showtime.getHourOfDay() : "" + showtime.getHourOfDay();
		    String minute = showtime.getMinuteOfHour() < 10 ? "0" + showtime.getMinuteOfHour() : "" + showtime.getMinuteOfHour();
		    String second = showtime.getSecondOfMinute() < 10 ? "0" + showtime.getSecondOfMinute() : "" + showtime.getSecondOfMinute();
		    String year = "" + showtime.getYear();
		    String month = showtime.getMonthOfYear() < 10 ? "0" + showtime.getMonthOfYear() : "" + showtime.getMonthOfYear();
		    String date = showtime.getDayOfMonth() < 10 ? "0" + showtime.getDayOfMonth() : "" + showtime.getDayOfMonth();
		    fullDate = year + "-" + month + "-" + date + " " + hour + ":" + minute + ":" + second;
		}

	    /** Get a JSON object representing the scheduled times **/
	    jsonArr = Functions.getJsonMovieSchedule(stmt);

	    // Get the list of urls for advertisements
	    ad_list = getAdUrls(stmt);

	    // get the seconds elapsed of the currently purchased movie
	    query = "CALL selectSecondsElapsedByShowingID(" + showtimeId + ");";
	    stmt = con.createStatement();
	    rs = stmt.executeQuery(query);
	    if (rs != null && rs.next())
		secondsElapsed = rs.getLong(1);

	    con.close();

	} catch (ClassNotFoundException e) {
	    trace("[LoginHandler] " + e);
	    e.printStackTrace();
	} catch (SQLException e) {
	    trace("[LoginHandler] " + e);
	    e.printStackTrace();
	} catch (JSONException e) {
	    trace("[LoginHandler] " + e);
	    e.printStackTrace();
	}

	/** Prepare the SFSObject to be sent to the client **/
	ISFSObject resObj = SFSObject.newInstance();
	resObj.putLong("sessionId", sessionId);
	resObj.putLong("userId", Long.parseLong(userID));
	resObj.putUtfString("error", error);

	// Ticket info
	resObj.putInt("showtimeId", showtimeId);
	resObj.putLong("showtime_millis", showtime == null ? -1 : showtime.getMillis());
	resObj.putInt("theaterNo", theaterNo);
	resObj.putInt("durationMin", durationMin);
	resObj.putUtfString("SHOWTIME_STRING", fullDate);

	// Movie Info
	resObj.putUtfString("movieTitle", movieTitle);
	resObj.putUtfString("movieDesc", movieDesc);
	resObj.putUtfString("movieLink", movieLink);
	resObj.putInt("movieId", movieId);
	resObj.putUtfString("imageLink", imageLink);
	resObj.putLong("SECONDS_ELAPSED", secondsElapsed);

	// Get the Jumbo cinema time
	DateTime dt = Utils.getJumboTime();
	resObj.putInt("year", dt.getYear());
	resObj.putInt("month", dt.getMonthOfYear());
	resObj.putInt("date", dt.getDayOfMonth());
	resObj.putInt("hour", dt.getHourOfDay());
	resObj.putInt("minute", dt.getMinuteOfHour());
	resObj.putInt("second", dt.getSecondOfMinute());
	resObj.putInt("millisecond", dt.getMillisOfSecond());

	// Showtime json string
	if (jsonArr != null)
	    resObj.putUtfString("showtimeJson", jsonArr.toString());

	if (ad_list != null)
	    resObj.putUtfStringArray("adList", ad_list);

	// Send it back
	send("loginRequest", resObj, sender);
    }

    private List<String> getAdUrls(Statement stmt) throws SQLException {
	String query = "SELECT * FROM advertisements";
	stmt.execute(query);

	ResultSet rs = stmt.getResultSet();

	List<String> ad_list = new ArrayList<String>();
	while (rs.next())
	    ad_list.add(rs.getString("ad_link"));

	return ad_list;
    }
}
