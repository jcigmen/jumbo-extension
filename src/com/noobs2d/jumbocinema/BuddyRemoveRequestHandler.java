package com.noobs2d.jumbocinema;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;

public class BuddyRemoveRequestHandler extends ExtensionRequestHandler {

    public static final String TAG = "BUDDY_REMOVE";

    @Override
    public void handleClientRequest(User sender, ISFSObject params) {
	final ISFSObject returnParams = SFSObject.newInstance();
	final String userID = params.getUtfString("USER_ID1");
	final String userIDToUnfriend = params.getUtfString("USER_ID2");
	final String usernameToUnfriend = params.getUtfString("USERNAME");

	try {
	    MySQLDatabase.connect();

	    // if this query yields a row, they are not yet friends
	    final String SELECT_BUDDY_QUERY = "CALL selectBuddyByUserID('" + userID + "', '" + userIDToUnfriend + "');";
	    ResultSet resultSet = MySQLDatabase.executeQuery(SELECT_BUDDY_QUERY);
	    boolean notYetFriends = !resultSet.next();

	    if (notYetFriends) {
		returnParams.putInt("FLAGS", Main.FAILED);
		returnParams.putUtfString("FLAG_FEED", "You are not yet friends with " + usernameToUnfriend + ".");
	    } else {
		// Remove user from friendlist
		String deleteBuddyRelationQuery = "CALL deleteBuddyRelation(" + userID + ", " + userIDToUnfriend + ");";
		MySQLDatabase.executeQuery(deleteBuddyRelationQuery);
		returnParams.putInt("FLAGS", Main.SUCCESS);
		returnParams.putUtfString("FLAG_FEED", "You are no longer friends with " + usernameToUnfriend + ".");
	    }

	    MySQLDatabase.disconnect();
	} catch (SQLException e) {
	    onExceptionEvent("BuddyRemoveRequestHandler", e, returnParams);
	} catch (ClassNotFoundException e) {
	    onExceptionEvent("BuddyRemoveRequestHandler", e, returnParams);
	}
	send(TAG, returnParams, sender);
    }
}
