package com.noobs2d.jumbocinema;

import com.noobs2d.jumbocinema.feedback.FeedbackMailer;
import com.noobs2d.jumbocinema.settings.SettingsRetrieval;
import com.noobs2d.jumbocinema.settings.SettingsSaver;
import com.noobs2d.jumbocinema.staffpicks.StaffPickSchedulesByUser;
import com.noobs2d.jumbocinema.staffpicks.TicketPurchasingHandler;
import com.noobs2d.jumbocinema.youtube.PlayingSchedules;
import com.noobs2d.jumbocinema.youtube.PlayingUserSchedules;
import com.noobs2d.jumbocinema.youtube.RoomCreator;
import com.noobs2d.jumbocinema.youtube.ScheduleCreator;
import com.noobs2d.jumbocinema.youtube.ScheduleExistenceChecker;
import com.noobs2d.jumbocinema.youtube.ScheduleJoiner;
import com.noobs2d.jumbocinema.youtube.Schedules;
import com.noobs2d.jumbocinema.youtube.UserSchedules;
import com.smartfoxserver.v2.core.SFSEventType;
import com.smartfoxserver.v2.extensions.SFSExtension;

public class Main extends SFSExtension {

    /** all purpose return flag used when an exception is thrown */
    public static final int ERROR = 2;

    /** all purpose return flag used when conditions for a specific operations are not met */
    public static final int FAILED = 1;

    /** used when determining on which DB to connect to */
    public static final boolean IS_PRODUCTION = true;

    /** all purpose return flag for successful operations */
    public static final int SUCCESS = 0;

    @Override
    public void init() {
	addRequestHandler(FeedbackMailer.TAG, FeedbackMailer.class);

	// settings handlers
	addRequestHandler(SettingsRetrieval.TAG, SettingsRetrieval.class);
	addRequestHandler(SettingsSaver.TAG, SettingsSaver.class);

	// youtube ext handlers
	addRequestHandler(ScheduleJoiner.TAG, ScheduleJoiner.class);
	addRequestHandler(RoomCreator.TAG, RoomCreator.class);
	addRequestHandler(PlayingSchedules.TAG, PlayingSchedules.class);
	addRequestHandler(PlayingUserSchedules.TAG, PlayingUserSchedules.class);
	addRequestHandler(ScheduleCreator.TAG, ScheduleCreator.class);
	addRequestHandler(ScheduleExistenceChecker.TAG, ScheduleExistenceChecker.class);
	addRequestHandler(Schedules.TAG, Schedules.class);
	addRequestHandler(UserSchedules.TAG, UserSchedules.class);

	addRequestHandler(BlockRemoveRequestHandler.TAG, BlockRemoveRequestHandler.class);
	addRequestHandler(BlockUserRequestHandler.TAG, BlockUserRequestHandler.class);
	addRequestHandler(BlockListRequestHandler.TAG, BlockListRequestHandler.class);
	addRequestHandler(BuddyAddRequestHandler.TAG, BuddyAddRequestHandler.class);
	addRequestHandler(BuddyRemoveRequestHandler.TAG, BuddyRemoveRequestHandler.class);
	addRequestHandler(TraceRequestHandler.TAG, TraceRequestHandler.class);
	addRequestHandler(UserListRequestHandler.TAG, UserListRequestHandler.class);
	addRequestHandler(VoidTicketsRequestHandler.TAG, VoidTicketsRequestHandler.class);
	addRequestHandler("BUDDY_ENLIST", BuddyListRequestHandler.class);
	addRequestHandler("loginRequest", LoginHandler.class);
	addRequestHandler("logoutRequest", LogoutReqHandler.class);
	addRequestHandler("saveAvatar", SaveAvatarHandler.class);
	addRequestHandler("updateRating", UpdateRatingHandler.class);
	addRequestHandler("emoticonAction", EmoticonActionHandler.class);
	addRequestHandler(TicketPurchasingHandler.TAG, TicketPurchasingHandler.class);
	addRequestHandler("getMovieInfo", MovieInfoHandler.class);
	addRequestHandler("getAvatar", GetAvatarHandler.class);
	addRequestHandler("getShowtimeSchedule", GetShowtimeSchedule.class);
	addRequestHandler("keepAlive", KeepAliveHandler.class);
	addRequestHandler(StaffPickSchedulesByUser.TAG, StaffPickSchedulesByUser.class);

	// Handles recording logouts / logins
	addEventHandler(SFSEventType.USER_DISCONNECT, UserDisconnectHandler.class);
	addEventHandler(SFSEventType.USER_LEAVE_ROOM, UserDisconnectHandler.class);
    }
}
