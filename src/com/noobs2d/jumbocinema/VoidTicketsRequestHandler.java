package com.noobs2d.jumbocinema;

import java.sql.SQLException;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;

public class VoidTicketsRequestHandler extends ExtensionRequestHandler {

    public static final String TAG = "VOID_TICKETS";

    @Override
    public void handleClientRequest(User sender, ISFSObject params) {
	final ISFSObject returnParams = SFSObject.newInstance();
	final String userID = params.getUtfString("USER_ID");

	try {
	    MySQLDatabase.connect();

	    final String query = "CALL deleteTicketsByUserID(" + userID + ");";
	    MySQLDatabase.execute(query);

	    MySQLDatabase.disconnect();
	} catch (SQLException e) {
	    onExceptionEvent("VoidTicketsRequestHandler", e, returnParams);
	} catch (ClassNotFoundException e) {
	    onExceptionEvent("VoidTicketsRequestHandler", e, returnParams);
	}
	send(TAG, returnParams, sender);
    }

}
