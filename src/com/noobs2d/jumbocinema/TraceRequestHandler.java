package com.noobs2d.jumbocinema;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;

public class TraceRequestHandler extends ExtensionRequestHandler {

    public static final String TAG = "TRACE";

    @Override
    public void handleClientRequest(User sender, ISFSObject params) {
	String message = params.getUtfString(TAG);
	trace("[TraceRequestHandler] " + message);
    }

}
