package com.noobs2d.jumbocinema;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import com.noobs2d.jumbocinema.classes.RatingObject;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

public class EmoticonActionHandler extends BaseClientRequestHandler{

	@Override
	public void handleClientRequest(User sender, ISFSObject params) 
	{		
		int showtimeId 		= params.getInt("showtimeId");
		int userId 			= params.getInt("userId");
		int emoticonId		= params.getInt("emoticon_id");	
		
		RatingObject ro 	= null;
		
		int insertStatus 	= -1;
				
		String error 					= "";
		String dbUrl = "jdbc:mysql://" + DbConfig.getDbIp() + "/" + DbConfig.getDb() + "?user=" + DbConfig.getUser() + "&password=" + DbConfig.getPassword();/*"jdbc:mysql://localhost/jumbotest?user=michael&password=thuglife";*/
		String sql   = "INSERT INTO emoticon_log (emoticonId, userId, showtimeId) VALUES (" + emoticonId + "," + userId + "," + showtimeId + ")";

		try 
		{
			Class.forName("com.mysql.jdbc.Driver");	
			Connection con = DriverManager.getConnection(dbUrl);
			Statement stmt = con.createStatement();
			
			insertStatus = stmt.executeUpdate(sql);
			
			con.close();
		
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			error = "classnotfound exception";
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			error = e.getMessage();
		}
		
		// Create the smartfox object to send back to the client
		ISFSObject resObj = SFSObject.newInstance(); 
		resObj.putDouble("status", insertStatus);
		resObj.putUtfString("error", error);
         
        // Send it back
        send("emoticonAction", resObj, sender);
	}
	
	

}
