package com.noobs2d.jumbocinema.classes;

public class RatingObject {

	private double _scalePos			= 5;
	private String _voteConditional 	= "";
	
	public RatingObject(double upvotes, double downvotes, boolean isUpVote)
	{
		findScalePosition(upvotes, downvotes, isUpVote);
	}
	
	public RatingObject(double upvotes, double downvotes)
	{
		findScalePosition(upvotes, downvotes, false);
	}
	
	public void findScalePosition(double upvotes, double downvotes, boolean isUpVote)
	{
		double totalvotes 	= upvotes + downvotes;
		
		double decimal 		= 0; // if upvotes == downvotes then the 0 will translate into a scalepos of 5
		double scaleRange 	= 10;
		double scalePos 	= scaleRange / 2;
		
		System.out.println("upvotes " + upvotes + " downvotes " + downvotes + " total votes " + totalvotes);
		
		if (upvotes > downvotes)
		{			
			decimal = (upvotes / totalvotes) * scaleRange;
		} 
		else if (downvotes > upvotes)
		{	
			decimal = 5 - ((downvotes / totalvotes) * scaleRange - 5);
		} else if (downvotes == upvotes)
			decimal = 5;
		
		_voteConditional = isUpVote ? ("upvotes = " + upvotes) : ("downvotes = " + downvotes);
		
		_scalePos	= Utils.roundToTwoDecimals(decimal);				
		
		
		System.out.println("total votes " + totalvotes + " decimal " + decimal + " scale pos " + _scalePos + " vote cond " + _voteConditional);
	}
	
	public void setScalePos(double scalePos)
	{
		_scalePos = scalePos;
	}

	public double getScalePos()
	{
		return _scalePos;
	}
	
	public void setVoteUpdateConditional(String voteConditional)
	{
		_voteConditional = voteConditional;
	}

	public String getVoteConditional()
	{
		return _voteConditional;
	}
}
