package com.noobs2d.jumbocinema.classes;

import java.sql.Timestamp;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

public class Utils {

    public static DateTimeZone dtz = DateTimeZone.forID("America/Asuncion");

    public static DateTime convertToTimeZone(long millis) {
	DateTime dts = new DateTime(millis);

	return new DateTime(dts.getYear(), dts.getMonthOfYear(), dts.getDayOfMonth(), dts.getHourOfDay(), dts.getMinuteOfHour(), dts.getSecondOfMinute(), 0, dtz);
    }

    public static DateTime convertToTimeZone(Timestamp ts) {
	DateTime dts = new DateTime(ts);

	return new DateTime(dts.getYear(), dts.getMonthOfYear(), dts.getDayOfMonth(), dts.getHourOfDay(), dts.getMinuteOfHour(), dts.getSecondOfMinute(), 0, dtz);
    }

    public static DateTime getJumboTime() {
	return new DateTime(dtz);
    }

    public static double roundToTwoDecimals(double num) {
	// couldn't return this in one statement, wouldn't return decimals. not sure why
	double roundNum = Math.round(num * 100);
	double total = roundNum / 100;

	return total;
    }

}
