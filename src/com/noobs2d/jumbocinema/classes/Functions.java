package com.noobs2d.jumbocinema.classes;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Functions {
	
	/**
	 * Returns a json array of movies that are scheduled to play,
	 * as well as movies that might currently be in progress, or
	 * have recently finished n.b the TIMESTAMPDIFF.
	 * 
	 * @param p_stmt
	 * @return
	 * @throws SQLException
	 * @throws JSONException
	 */
	public static JSONArray getJsonMovieSchedule(Statement p_stmt) throws SQLException, JSONException
	{
		String hoursAgo	= "4";
		String query 	= "SELECT s.idshowtimes, s.date, s.theatre, m.title, m.description, m.imageLink, g.genre, d.firstName, d.lastName FROM showtimes s " +
						"INNER JOIN movies m ON s.movieId = m.movieId " +
						"INNER JOIN genres g ON m.genre = g.idgenres " +
						"INNER JOIN directors d ON d.iddirectors = m.director " +
						"WHERE s.date > NOW( ) OR TIMESTAMPDIFF( HOUR , s.date, NOW( ) ) < " + hoursAgo + " ORDER BY DATE ASC";
		p_stmt.executeQuery(query);
		
		JSONArray json 			= new JSONArray();
		ResultSet rs 			= p_stmt.getResultSet();
		ResultSetMetaData rsmd 	= rs.getMetaData();
		
		/* Implementation taken from http://stackoverflow.com/questions/6514876/most-effecient-conversion-of-resultset-to-json */
		while(rs.next()) {
		      int numColumns = rsmd.getColumnCount();
		      JSONObject obj = new JSONObject();

		      for (int i=1; i<numColumns+1; i++) 
		      {
		        String column_name = rsmd.getColumnName(i);

		        if(rsmd.getColumnType(i)==java.sql.Types.ARRAY){
		         obj.put(column_name, rs.getArray(column_name));
		        }
		        else if(rsmd.getColumnType(i)==java.sql.Types.BIGINT){
		         obj.put(column_name, rs.getInt(column_name));
		        }
		        else if(rsmd.getColumnType(i)==java.sql.Types.BOOLEAN){
		         obj.put(column_name, rs.getBoolean(column_name));
		        }
		        else if(rsmd.getColumnType(i)==java.sql.Types.BLOB){
		         obj.put(column_name, rs.getBlob(column_name));
		        }
		        else if(rsmd.getColumnType(i)==java.sql.Types.DOUBLE){
		         obj.put(column_name, rs.getDouble(column_name)); 
		        }
		        else if(rsmd.getColumnType(i)==java.sql.Types.FLOAT){
		         obj.put(column_name, rs.getFloat(column_name));
		        }
		        else if(rsmd.getColumnType(i)==java.sql.Types.INTEGER){
		         obj.put(column_name, rs.getInt(column_name));
		        }
		        else if(rsmd.getColumnType(i)==java.sql.Types.NVARCHAR){
		         obj.put(column_name, rs.getNString(column_name));
		        }
		        else if(rsmd.getColumnType(i)==java.sql.Types.VARCHAR){
		         obj.put(column_name, rs.getString(column_name));
		        }
		        else if(rsmd.getColumnType(i)==java.sql.Types.TINYINT){
		         obj.put(column_name, rs.getInt(column_name));
		        }
		        else if(rsmd.getColumnType(i)==java.sql.Types.SMALLINT){
		         obj.put(column_name, rs.getInt(column_name));
		        }
		        else if(rsmd.getColumnType(i)==java.sql.Types.DATE){
		         obj.put(column_name, rs.getDate(column_name));
		        }
		        else if(rsmd.getColumnType(i)==java.sql.Types.TIMESTAMP){
		        obj.put(column_name, rs.getTimestamp(column_name));   
		        }
		        else{
		         obj.put(column_name, rs.getObject(column_name));
		        }
		      }

		      json.put(obj);
		    }		
		
		return json;
	}
	
}
