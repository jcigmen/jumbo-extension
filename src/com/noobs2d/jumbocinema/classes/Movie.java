package com.noobs2d.jumbocinema.classes;

import java.sql.Timestamp;

import org.joda.time.DateTime;


public class Movie {
	
	private double _rating;
	private int _movieId;
	private int _showtimeId;
	private long _showtime;
	private long _timeToStart;
	private long _movieLength;
	private RatingObject _ro;
	private String _movieTitle;
	private String _status; // before, during
	private String _youtubeLink;
	
	public Movie(int movieId, int showtimeId, String movieTitle, double rating, long showtime, long timeToStart, long movieLength, int upvotes, int downvotes, String status, String youtubeLink)
	{
		_movieId 		= movieId;
		_movieTitle 	= movieTitle;
		_movieLength 	= movieLength;
		_showtime 		= showtime;
		_showtimeId 	= showtimeId;
		_timeToStart 	= timeToStart;
		_ro 			= new RatingObject(upvotes, downvotes);
		_status 		= status;
		_youtubeLink 	= youtubeLink;
	}
	
	public String getMovieTitle()
	{
		return _movieTitle;
	}
	
	public int getMovieId()
	{
		return _movieId;
	}
	
	public int getShowtimeId()
	{
		return _showtimeId;
	}
			
	public long getShowTime()
	{
		return _showtime;
	}
	
	public long getTimeToShowtime()
	{
		return _timeToStart;
	}
	
	public long getMovieLength()
	{
		return _movieLength;
	}
	
	public RatingObject getRatingObject()
	{
		return _ro;
	}
	
	public String getStatus()
	{
		return _status;
	}
	
	public String getLink()
	{
		return _youtubeLink;
	}
}
