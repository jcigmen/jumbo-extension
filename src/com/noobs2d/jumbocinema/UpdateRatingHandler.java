package com.noobs2d.jumbocinema;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.noobs2d.jumbocinema.classes.RatingObject;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

public class UpdateRatingHandler extends BaseClientRequestHandler{

	@Override
	public void handleClientRequest(User sender, ISFSObject params) 
	{		
		boolean voteType 	= params.getBool("voteType"); // true = upVote, false = downVote
		int movieId 		= params.getInt("movieId");
		int showtimeId 		= params.getInt("showtimeId");
		int userId 			= params.getInt("userId");	
		
		RatingObject ro 	= null;
		
		int insertStatus 	= -1;
		int updateStatus 	= -1;
				
		String error 					= "";
		String dbUrl = "jdbc:mysql://" + DbConfig.getDbIp() + "/" + DbConfig.getDb() + "?user=" + DbConfig.getUser() + "&password=" + DbConfig.getPassword();/*"jdbc:mysql://localhost/jumbotest?user=michael&password=thuglife";*/
		String selectCurrentRatingSql 	= "SELECT ratings.id, ratings.upvotes AS upvotes, ratings.downvotes AS downvotes FROM ratings INNER JOIN movies ON movies.ratingId = ratings.id WHERE movies.movieId = " + movieId;
		System.out.println(selectCurrentRatingSql);	
		try 
		{
			Class.forName("com.mysql.jdbc.Driver");	
			Connection con = DriverManager.getConnection(dbUrl);
			Statement stmt = con.createStatement();
			
			stmt.executeQuery(selectCurrentRatingSql);
			
			ResultSet rs = stmt.getResultSet();
			if (rs != null && rs.next()) 
			{				
				// Do some math to calculate the new rating average and update existing values
				double upvotes 		= (double) (rs.getInt("upvotes") 		+ (voteType ? 1 : 0));
				double downvotes 	= (double) (rs.getInt("downvotes") 		+ (voteType ? 0 : 1));
				int ratingId 		= rs.getInt("id");
				
				// grab the rating object that contains the sql conditional along with the scale position
				ro = new RatingObject(upvotes, downvotes, voteType);
				
				System.out.println("scale position " + ro.getScalePos() + " conditional " + ro.getVoteConditional());
				
				// Update the DB with the new rating 			
				String updateRatingSql = "UPDATE ratings SET " + ro.getVoteConditional() + ", rating=" + ro.getScalePos() + " WHERE id = " + ratingId;
				System.out.println(updateRatingSql);
				
				// Execute Query
				stmt.executeUpdate(updateRatingSql);
				updateStatus = stmt.getUpdateCount();
				
				// Insert a new record into the ratings log table
				String insertRatingLog = "INSERT INTO ratingsLog (showtimeId, userId, ratingId, voteType) VALUES (" + 
						showtimeId + ", " + userId + ", " + ratingId + ", '" + ( voteType ? "up" : "down") + "' )";
				System.out.println(insertRatingLog);

				// Execute Query
				insertStatus = stmt.executeUpdate(insertRatingLog);	
				
				// Print statements
				System.out.println("Rating Log Insert " + insertStatus);
			}			
			
			con.close();
		
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			error = "classnotfound exception";
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			error = e.getMessage();
		}
		
		// Create the smartfox object to send back to the client
		ISFSObject resObj = SFSObject.newInstance(); 
		resObj.putDouble("rating", ro.getScalePos());
		resObj.putInt("updateStatus", updateStatus);
		resObj.putInt("insertStatus", insertStatus);
		resObj.putUtfString("error", error);
         
        // Send it back
        send("updateRating", resObj, sender);
	}
	
	

}
