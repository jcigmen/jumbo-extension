package com.noobs2d.jumbocinema;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSArray;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

public class BuddyListRequestHandler extends BaseClientRequestHandler {

    @Override
    public void handleClientRequest(User sender, ISFSObject params) {
	final String USER_ID = params.getUtfString("USER_ID");

	String dbUrl = "jdbc:mysql://" + DbConfig.getDbIp() + "/" + DbConfig.getDb() + "?user=" + DbConfig.getUser() + "&password=" + DbConfig.getPassword();

	try {
	    Class.forName("com.mysql.jdbc.Driver");
	    Connection con = DriverManager.getConnection(dbUrl);

	    Statement stmt = con.createStatement();

	    final String BUDDIES_YOU_ADDED_QUERY = "CALL getBuddiesYouAdded('" + USER_ID + "');";
	    stmt.executeQuery(BUDDIES_YOU_ADDED_QUERY);
	    ResultSet rs = stmt.getResultSet();
	    SFSArray buddyNames = new SFSArray();
	    ArrayList<Long> buddyIDs = new ArrayList<Long>();
	    while (rs != null && rs.next()) {
		Long id = rs.getLong("buddy");
		String name = rs.getString("firstName") + " " + rs.getString("lastName");
		buddyIDs.add(id);
		buddyNames.addUtfString(name);
	    }
	    final String BUDDIES_WHO_ADDED_YOU_QUERY = "CALL getBuddiesWhoAddedYou('" + USER_ID + "');";
	    rs = stmt.executeQuery(BUDDIES_WHO_ADDED_YOU_QUERY);
	    while (rs != null && rs.next()) {
		Long value = rs.getLong("buddy");
		String name = rs.getString("firstName") + " " + rs.getString("lastName");
		buddyIDs.add(value);
		buddyNames.addUtfString(name);
	    }
	    con.close();

	    ISFSObject responseObject = SFSObject.newInstance();
	    responseObject.putLongArray("BUDDY_LIST_IDS", buddyIDs);
	    responseObject.putSFSArray("BUDDY_LIST_NAMES", buddyNames);
	    send("BUDDY_ENLIST", responseObject, sender);
	} catch (SQLException e) {
	    trace("[BuddyListRequestHandler] " + e);
	    e.printStackTrace();
	} catch (ClassNotFoundException e) {
	    trace("[BuddyListRequestHandler]" + e);
	    e.printStackTrace();
	}
    }

}
