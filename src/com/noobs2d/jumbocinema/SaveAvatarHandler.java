package com.noobs2d.jumbocinema;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

/**
 * When a user logs in, if they haven't logged in before, they will be added to the database,
 * otherwise we'll just log their login time.
 * 
 * @author michael
 */
public class SaveAvatarHandler extends BaseClientRequestHandler {

    @Override
    public void handleClientRequest(User sender, ISFSObject params) {
	int hairId = -1, outfitId = -1, skinId = -1, avatarId = -1, hatId = 1;

	long userId = params.getLong("userId");
	String hairFileName = params.getUtfString("hairFileName");
	String outfitFileName = params.getUtfString("outfitFileName");
	String skinFileName = params.getUtfString("skinFileName");
	String gender = params.getUtfString("gender");
	String hatFileName = params.getUtfString("hatFileName");

	trace("[SaveAvatarHandler] hairFileName: " + hairFileName);
	trace("[SaveAvatarHandler] outfitFileName: " + outfitFileName);
	trace("[SaveAvatarHandler] skinFileName: " + skinFileName);
	trace("[SaveAvatarHandler] gender: " + gender);
	trace("[SaveAvatarHandler] hatFileName: " + hatFileName);

	String dbUrl = "jdbc:mysql://" + DbConfig.getDbIp() + "/" + DbConfig.getDb() + "?user=" + DbConfig.getUser() + "&password=" + DbConfig.getPassword();/*
																			      * "jdbc:mysql://localhost/jumbotest?user=michael&password=thuglife"
																			      * ;
																			      */

	String error = "";
	try {
	    Class.forName("com.mysql.jdbc.Driver");
	    Connection con = DriverManager.getConnection(dbUrl);
	    Statement stmt = con.createStatement();

	    /** ***** Get Hair Id ***** **/
	    String hairIdSelect = "SELECT idavatarHair FROM " + DbConfig.getDb() + ".avatarhair WHERE hairFileName = '" + hairFileName + "'";
	    stmt.executeQuery(hairIdSelect);

	    ResultSet rs = stmt.getResultSet();
	    if (rs != null && rs.next())
		hairId = rs.getInt(1);
	    trace("[SaveAvatarHandler] HairID: " + hairId);

	    /** ***** Get Outfit Id ***** **/
	    String outfitIdSelect = "SELECT idavatarOutfit FROM " + DbConfig.getDb() + ".avataroutfit WHERE outfitFileName = '" + outfitFileName + "'";
	    stmt.executeQuery(outfitIdSelect);

	    rs = stmt.getResultSet();
	    if (rs != null && rs.next())
		outfitId = rs.getInt(1);
	    trace("[SaveAvatarHandler] outfitID: " + outfitId);

	    /** ***** Get Skin Id ***** **/
	    String skinIdSelect = "SELECT idavatarSkins FROM " + DbConfig.getDb() + ".avatarskins WHERE skinFileName = '" + skinFileName + "'";
	    stmt.executeQuery(skinIdSelect);

	    rs = stmt.getResultSet();
	    if (rs != null && rs.next())
		skinId = rs.getInt(1);
	    trace("[SaveAvatarHandler] SkinID: " + skinId);

	    /** ***** Get Hat Id ***** **/
	    String hatIdSelect = "SELECT idavatarhat FROM avatarhat WHERE hatFileName = '" + hatFileName + "'";
	    stmt.executeQuery(hatIdSelect);

	    rs = stmt.getResultSet();
	    if (rs != null && rs.next())
		hatId = rs.getInt(1);
	    trace("[SaveAvatarHandler] HatID: " + hatId);

	    trace("[SaveAvatarHandler] Checking if user already has an avatar...");
	    /** ***** Check to see if user already has an avatar ***** **/
	    String curAvatarSelect = "SELECT avatarId FROM users WHERE idusers = " + userId;
	    rs = stmt.executeQuery(curAvatarSelect);

	    if (rs != null && rs.next())
		avatarId = rs.getInt("avatarId");

	    // The user doesn't have an avatar yet
	    if (avatarId == 0) {
		trace("[SaveAvatarHandler] The user does not have an avatar yet.");
		// Insert the avatar specs and grab the created ID
		String insertAvatarQuery = "INSERT INTO avatars(hairId, outfitId, skinId, gender, hatId) VALUES ('" + hairId + "', '" + outfitId + "', '" + skinId + "','" + gender + "', '" + hatId + "')";
		stmt.executeUpdate(insertAvatarQuery, Statement.RETURN_GENERATED_KEYS);

		rs = stmt.getGeneratedKeys();
		if (rs != null && rs.next()) {
		    avatarId = rs.getInt(1);

		    String updateUserAvatar = "UPDATE users SET avatarId = " + avatarId + " WHERE idusers = " + userId;
		    stmt.executeUpdate(updateUserAvatar);
		}
	    } else {
		trace("[SaveAvatarHandler] Updating existing avatar...");
		// We are updating an existing avatar
		String insertAvatarQuery = "UPDATE avatars SET hairId = " + hairId + ", outfitId = " + outfitId + ", skinId = " + skinId + ", gender = '" + gender + "', hatId = " + hatId + " WHERE idavatars = " + avatarId;
		stmt.executeUpdate(insertAvatarQuery);
	    }

	    con.close();

	} catch (ClassNotFoundException e) {
	    trace("[SaveAvatarHandler] " + e);
	    e.printStackTrace();
	} catch (SQLException e) {
	    trace("[SaveAvatarHandler] " + e);
	    e.printStackTrace();
	}
    }

}
