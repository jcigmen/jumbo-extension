package com.noobs2d.jumbocinema;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import com.noobs2d.jumbocinema.classes.Movie;
import com.noobs2d.jumbocinema.classes.Utils;

public class DatabaseConnect {

    public Movie getMovie(int theaterNo) {
	String dbUrl = "jdbc:mysql://" + DbConfig.getDbIp() + "/" + DbConfig.getDb() + "?user=" + DbConfig.getUser() + "&password=" + DbConfig.getPassword();
	String query = "SELECT showtimes.idshowtimes AS showtimeId, showtimes.date AS date," + "movies.movieId as id, movies.title as title, movies.youtube AS youtube, movies.duration AS duration, ratings.upvotes AS upvotes, ratings.downvotes AS downvotes " + "FROM showtimes "
		+ "INNER JOIN movies ON showtimes.movieId = movies.movieId " + "INNER JOIN ratings ON movies.ratingId = ratings.id " + "WHERE theatre = " + theaterNo + //" AND " +
		" ORDER BY DATE ASC ";

	System.out.println(query);

	// Init variables
	int movieId = -1;
	int showtimeId = -1;
	int upvotes = -1;
	int downvotes = -1;

	long timeToStartInterval = -1; // will be positive if it's before the movie, negative if it's during
	double rating = -1;
	double movieLength = -1;

	Movie movie = null;
	DateTime startTime = null;
	DateTime endTime = null;
	int duration = -1; // Minutes
	String movieTitle = null;
	String status = ""; // before, during
	String youtubeLink = "";

	try {
	    Class.forName("com.mysql.jdbc.Driver");
	    Connection con = DriverManager.getConnection(dbUrl);

	    Statement stmt = con.createStatement();
	    ResultSet rs = stmt.executeQuery(query);

	    // Find which movie should be playing, or should be
	    // queued up
	    while (rs.next()) {
		upvotes = rs.getInt("upvotes");
		downvotes = rs.getInt("downvotes");
		movieId = rs.getInt("id");
		showtimeId = rs.getInt("showtimeId");
		movieTitle = rs.getString("title");
		duration = rs.getInt("duration");
		youtubeLink = rs.getString("youtube");

		startTime = Utils.convertToTimeZone(rs.getTimestamp("date"));

		Duration dur = new Duration(Utils.getJumboTime().getMillis(), startTime.getMillis());
		timeToStartInterval = dur.getStandardSeconds();

		// Add duration to showtime to get the endTime
		endTime = startTime.plusMinutes(duration);
		DateTime currentTime = Utils.getJumboTime();

		// Before the showtime. Since the movies are ordered in
		// ascending order, this will be the next movie
		if (currentTime.isBefore(startTime)) {
		    status = "before";
		    break;
		}
		// After the movie, continue going through the results
		else if (currentTime.isAfter(endTime)) {

		}
		// During the movie. Send back a current spot in the movie
		else if (currentTime.isAfter(startTime) && currentTime.isBefore(endTime)) {
		    status = "during";
		    break;
		}
	    }

	    con.close();

	    movie = new Movie(movieId, showtimeId, movieTitle, rating, startTime.getMillis(), timeToStartInterval, new Double(duration).longValue(), upvotes, downvotes, status, youtubeLink);
	} catch (ClassNotFoundException e) {
	    e.printStackTrace();
	} catch (SQLException e) {
	    e.printStackTrace();
	}

	return movie;
    }
}