package com.noobs2d.jumbocinema.settings;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.noobs2d.jumbocinema.ExtensionRequestHandler;
import com.noobs2d.jumbocinema.MySQLDatabase;
import com.noobs2d.jumbocinema.settings.SettingsSaver.SettingsKeys;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;

/**
 * @author Julious Cious Igmen <jcigmen@gmail.com>
 */
public class SettingsRetrieval extends ExtensionRequestHandler {

    public static final String TAG = "RETRIEVE_SETTINGS";

    @Override
    public void handleClientRequest(User sender, ISFSObject params) {
	ISFSObject response = SFSObject.newInstance();
	String userID = params.getUtfString("USER_ID");

	try {
	    MySQLDatabase.connect();

	    String query = "SELECT activity_sharing_enabled, music_enabled, sound_enabled FROM settings WHERE user_id = '" + userID + "';";
	    ResultSet resultSet = MySQLDatabase.execute(query);

	    while (resultSet.next()) {
		int activitySharingEnabled = resultSet.getInt("activity_sharing_enabled");
		int musicEnabled = resultSet.getInt("music_enabled");
		int soundEnabled = resultSet.getInt("sound_enabled");

		response.putBool(SettingsKeys.ACTIVITY_SHARING_ENABLED, activitySharingEnabled == 1);
		response.putBool(SettingsKeys.MUSIC_ENABLED, musicEnabled == 1);
		response.putBool(SettingsKeys.SOUND_ENABLED, soundEnabled == 1);
	    }

	    // if this point is reached, then no errors were encountered
	    // attach a success feed for the response
	    attachSuccessFeedToResponse(response, "Settings retrieved.");

	    MySQLDatabase.disconnect();
	} catch (SQLException e) {
	    attachFailedFeedToResponse(response, e.getMessage());
	} catch (ClassNotFoundException e) {
	    attachFailedFeedToResponse(response, e.getMessage());
	}

	send(TAG, response, sender);
    }

}
