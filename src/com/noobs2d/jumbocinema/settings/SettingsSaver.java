package com.noobs2d.jumbocinema.settings;

import java.sql.SQLException;

import com.noobs2d.jumbocinema.ExtensionRequestHandler;
import com.noobs2d.jumbocinema.MySQLDatabase;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;

/**
 * @author Julious Cious Igmen <jcigmen@gmail.com>
 */
public class SettingsSaver extends ExtensionRequestHandler {

    public static final String TAG = "SAVE_SETTINGS";

    public static class SettingsKeys {

	public static final String ACTIVITY_SHARING_ENABLED = "ACTIVITY_SHARING_ENABLED";
	public static final String MUSIC_ENABLED = "MUSIC_ENABLED";
	public static final String SOUND_ENABLED = "SOUND_ENABLED";
    }

    @Override
    public void handleClientRequest(User sender, ISFSObject params) {
	ISFSObject response = SFSObject.newInstance();
	String userID = params.getUtfString("USER_ID");
	boolean activitySharingEnabled = params.containsKey(SettingsKeys.ACTIVITY_SHARING_ENABLED) ? params.getBool(SettingsKeys.ACTIVITY_SHARING_ENABLED) : true;
	boolean musicEnabled = params.containsKey(SettingsKeys.MUSIC_ENABLED) ? params.getBool(SettingsKeys.MUSIC_ENABLED) : true;
	boolean soundEnabled = params.containsKey(SettingsKeys.SOUND_ENABLED) ? params.getBool(SettingsKeys.SOUND_ENABLED) : true;

	try {
	    MySQLDatabase.connect();

	    String query = "CALL settingsSave('" + userID + "', " + (activitySharingEnabled ? "1" : "0") + ", " + (musicEnabled ? "1" : "0") + ", " + (soundEnabled ? "1" : "0") + ");";
	    MySQLDatabase.execute(query);

	    // if this point is reached, then no errors were encountered
	    // attach a success feed for the response
	    attachSuccessFeedToResponse(response, "Settings saved.");

	    MySQLDatabase.disconnect();
	} catch (SQLException e) {
	    attachFailedFeedToResponse(response, e.getMessage());
	} catch (ClassNotFoundException e) {
	    attachFailedFeedToResponse(response, e.getMessage());
	}

	send(TAG, response, sender);
    }
}
