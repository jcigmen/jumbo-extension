package com.noobs2d.jumbocinema;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import com.noobs2d.jumbocinema.classes.Functions;
import com.smartfoxserver.v2.core.ISFSEvent;
import com.smartfoxserver.v2.core.SFSEventParam;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.exceptions.SFSException;
import com.smartfoxserver.v2.exceptions.SFSLoginException;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;
import com.smartfoxserver.v2.extensions.BaseServerEventHandler;

import org.json.*;

/**
 * When a user logs in, if they haven't logged in before, 
 * they will be added to the database, otherwise we'll 
 * just log their login time.
 * 
 * @author michael
 *
 */
public class KeepAliveHandler extends BaseClientRequestHandler{

	@Override
	public void handleClientRequest(User sender, ISFSObject params) 
	{
		/** Prepare the SFSObject to be sent to the client **/
		ISFSObject resObj = SFSObject.newInstance(); 
       	resObj.putUtfString("ret", "kept alive!");
         
        // Send it back
        send("keepalive", resObj, sender);		
	}
}
