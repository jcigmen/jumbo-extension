package com.noobs2d.jumbocinema.youtube;

import java.sql.SQLException;

import com.noobs2d.jumbocinema.ExtensionRequestHandler;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;

/**
 * Retrieves all the scheduled YouTube videos by a certain user. The returned ISFSObject that 
 * will be used in the front end shall be composed of the following data:
 * 
 * "ROW_COUNT" (int) - The amount of rows yield by this request. This is used to reference the index
 * of the entries we've got so if we received 42 rows from the query, we can use 0-41 as indexes
 * for the next data:
 * 
 * "VIDEO_ID[X]" (UtfString) - the id of the video. X is the index from the row. (i.e., VIDEO_ID[12] is the 13th
 * row in the query's result
 * 
 * "NAMES[X]" (UtfStringArray) - the names of the reservees, ordered alphabetically by first name
 * 
 * "USER_IDS[X]" - (UtfStringArray) - user IDs of the reservees
 * 
 * "USER_COUNT[X]" (int) - quantity of users that are going to watch this schedule. also the length of USER_IDS
 *
 * "REMAINING[X]" (int) - the amount of seconds before the schedule is desired to play
 * 
 * "ROOM_NAME[X]" (UtfString)
 * 
 * back in the front end, the programmer will loop through the ISFSObject, using the ROW_COUNT as the 
 * counter flag, telling his loop to end.
 * 
 * Required data from the received ISFSObject:
 * 
 * USER_ID (UtfString)
 * 
 * @author MrUseL3tter
 */
public class UserSchedules extends ExtensionRequestHandler {

    public static final String TAG = "YOUTUBE_GET_SCHEDULES_BY_USER";

    @Override
    public void handleClientRequest(User sender, ISFSObject params) {
	final ISFSObject returnParams = SFSObject.newInstance();
	final String userID = params.getUtfString("USER_ID");

	try {
	    String query = "CALL youTubeGetSchedulesByUserID('" + userID + "');";
	    int schedulesCount = YouTubeUtils.putStandardScheduleDataToSFSObject(getParentExtension(), returnParams, query);
	    if (schedulesCount > 0)
		YouTubeUtils.putDetailsFromScheduleToSFSObject(returnParams, schedulesCount);
	} catch (SQLException e) {
	    onExceptionEvent(getClass().getName(), e, returnParams);
	} catch (ClassNotFoundException e) {
	    onExceptionEvent(getClass().getName(), e, returnParams);
	}
	send(TAG, returnParams, sender);
    }
}
