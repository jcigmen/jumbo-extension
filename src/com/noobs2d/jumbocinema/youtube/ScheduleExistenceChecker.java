package com.noobs2d.jumbocinema.youtube;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.noobs2d.jumbocinema.ExtensionRequestHandler;
import com.noobs2d.jumbocinema.Main;
import com.noobs2d.jumbocinema.MySQLDatabase;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;

/**
 * Checks whether a certain schedule (user_id, video_id & watch date (YYYY-MM-DD HH:MM:SS)) already exists.
 * Returns a 0 (failed) if the schedule has ended or not existing, otherwise 1 (success) and the following:
 * 
 * VIDEO_ID, UtfString
 * WATCH_DATE, UtfString
 * DURATION, int
 * PLAYING, Bool
 * REMAINING, int
 * ROOM_NAME, UtfString
 * 
 * Take note that REMAINING will always be positive. If PLAYING is 0, the schedule isn't playing yet and
 * the value of REMAINING indicates the seconds left before the schedule plays, otherwise the value of REMAINING
 * indicates the seconds elapsed since the schedule has started playing.
 * 
 * @author Julious Cious Igmen <jcigmen@gmail.com>
 */
public class ScheduleExistenceChecker extends ExtensionRequestHandler {

    public static final String TAG = "YOUTUBE_SCHEDULE_BY_DETAILS";

    @Override
    public void handleClientRequest(User sender, ISFSObject params) {
	final ISFSObject returnParams = SFSObject.newInstance();
	final String userID = params.getUtfString("USER_ID");
	final String videoID = params.getUtfString("VIDEO_ID");
	final String watchDate = params.getUtfString("WATCH_DATE");
	try {
	    MySQLDatabase.connect();

	    String query = "CALL youTubeGetSchedulesByFullDetails('" + userID + "', '" + videoID + "', '" + watchDate + "');";
	    ResultSet resultSet = MySQLDatabase.executeQuery(query);
	    boolean hasResults = false;

	    while (resultSet.next()) {
		hasResults = true;
		returnParams.putUtfString("VIDEO_ID", resultSet.getString("video_id"));
		returnParams.putUtfString("WATCH_DATE", YouTubeUtils.getFormattedDateTimeFromResultSet(resultSet, "watch_date"));
		returnParams.putInt("DURATION", resultSet.getInt("duration"));
		returnParams.putBool("PLAYING", resultSet.getInt("playing") == 1);
		returnParams.putInt("REMAINING", resultSet.getInt("seconds_before_showtime"));
		returnParams.putUtfString("ROOM_NAME", resultSet.getString("ROOM_NAME"));
	    }

	    if (hasResults) {
		returnParams.putInt("FLAGS", Main.SUCCESS);
		returnParams.putUtfString("FLAG_FEED", "An existing (playing/not playing yet) schedule exists!");
	    } else {
		returnParams.putInt("FLAGS", Main.FAILED);
		returnParams.putUtfString("FLAG_FEED", "No schedules fetched.");
	    }

	    MySQLDatabase.disconnect();
	} catch (SQLException e) {
	    onExceptionEvent(getClass().getName(), e, returnParams);
	} catch (ClassNotFoundException e) {
	    onExceptionEvent(getClass().getName(), e, returnParams);
	}
	send(TAG, returnParams, sender);
    }
}
