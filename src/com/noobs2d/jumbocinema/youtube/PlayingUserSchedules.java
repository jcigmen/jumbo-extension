package com.noobs2d.jumbocinema.youtube;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

import com.noobs2d.jumbocinema.ExtensionRequestHandler;
import com.noobs2d.jumbocinema.MySQLDatabase;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;

/**
 * Gets all the schedules by a user that is currently playing.
 * 
 * "ROW_COUNT" (int) - The amount of rows yield by this request. This is used to reference the index
 * of the entries we've got so if we received 42 rows from the query, we can use 0-41 as indexes
 * for the next data:
 * 
 * "VIDEO_ID[X]" (UtfString) - the id of the video. X is the index from the row. (i.e., VIDEO_ID[12] is the 13th
 * row in the query's result
 * 
 * "NAMES[X]" (UtfStringArray) - the names of the reservees, ordered alphabetically by first name
 * 
 * "USER_IDS[X]" - (UtfStringArray) - user IDs of the reservees
 * 
 * "USER_COUNT[X]" (int) - quantity of users that are going to watch this schedule. also the length of USER_IDS
 *
 * "REMAINING[X]" (int) - the amount of seconds before the schedule is desired to play
 * 
 * "ROOM_NAME[X]" (UtfString)
 * 
 * back in the front end, the programmer will loop through the ISFSObject, using the ROW_COUNT as the 
 * counter flag, telling his loop to end.
 * 
 * Required data from the received ISFSObject:
 * 
 * USER_ID (UtfString)
 * 
 * @author MrUseL3tter
 */
public class PlayingUserSchedules extends ExtensionRequestHandler {

    public static final String TAG = "YOUTUBE_GET_PLAYING_SCHEDULES_BY_USER";

    @Override
    public void handleClientRequest(User sender, ISFSObject params) {
	final ISFSObject returnParams = SFSObject.newInstance();
	final String userID = params.getUtfString("USER_ID");

	try {
	    MySQLDatabase.connect();

	    String query = "CALL youTubeGetSchedulesPlayingByUserID('" + userID + "');";
	    ResultSet resultSet = MySQLDatabase.executeQuery(query);
	    ArrayList<String> watchDates = new ArrayList<String>();
	    int index = 0;
	    while (resultSet.next()) {
		String videoID = resultSet.getString("video_id");
		int secondsRemaining = resultSet.getInt("seconds_remaining");
		Date date = resultSet.getDate("watch_date");
		Time time = resultSet.getTime("watch_date");
		String watchDate = date.toString() + " " + time.toString();
		watchDates.add(watchDate);
		returnParams.putUtfString("VIDEO_ID[" + index + "]", videoID);
		returnParams.putInt("REMAINING[" + index + "]", secondsRemaining);
		index++;
	    }
	    returnParams.putInt("ROW_COUNT", index);

	    MySQLDatabase.disconnect();

	    // we loop throughout our initial results to request for details about them since
	    // that query doesn't provide the names of the users
	    // 
	    // BUT ONLY IF THERE WAS A RESULT FROM THE PREVIOUS QUERY!
	    if (index > 0)
		for (int i = 0; i < index; i++) {
		    MySQLDatabase.connect();

		    String videoID = returnParams.getUtfString("VIDEO_ID[" + i + "]");
		    String watchDate = watchDates.get(i);

		    query = "CALL youTubeGetSchedulesUsers('" + videoID + "', '" + watchDate + "');";
		    resultSet = MySQLDatabase.executeQuery(query);
		    List<String> ids = new ArrayList<String>();
		    List<String> names = new ArrayList<String>();
		    int subIndex = 0;
		    while (resultSet.next()) {
			String id = resultSet.getString("user_id");
			String firstName = resultSet.getString("first_name");
			String lastName = resultSet.getString("last_name");

			ids.add(id);
			names.add(firstName + " " + lastName);

			subIndex++;
		    }

		    String roomName = watchDate.substring(11).substring(0, 5);
		    roomName = roomName.substring(0, 2) + roomName.substring(3, 5);
		    roomName = videoID + roomName;
		    returnParams.putUtfStringArray("USER_IDS[" + i + "]", ids);
		    returnParams.putUtfStringArray("NAMES[" + i + "]", names);
		    returnParams.putInt("USER_COUNT[" + i + "]", subIndex);
		    returnParams.putUtfString("ROOM_NAME[" + i + "]", roomName);
		    returnParams.putBool("PLAYING[" + i + "]", true);

		    MySQLDatabase.disconnect();
		}
	} catch (SQLException e) {
	    onExceptionEvent(getClass().getName(), e, returnParams);
	} catch (ClassNotFoundException e) {
	    onExceptionEvent(getClass().getName(), e, returnParams);
	}
	send(TAG, returnParams, sender);
    }

}
