package com.noobs2d.jumbocinema.youtube;

/**
 * A schedule with limited info. This is solely for the timer thread in {@link ScheduleTimer}.
 * 
 * @author Julious Cious Igmen <jcigmen@gmail.com>
 */
public class RawSchedule implements ISchedule {

    private int duration;
    private boolean playing;
    private int secondsRemaining;
    private String watchDate;
    private boolean removable;

    public RawSchedule(int duration, boolean playing, int secondsRemaining, String watchDate) {
	this.duration = duration;
	this.playing = playing;
	this.secondsRemaining = secondsRemaining;
	this.watchDate = watchDate;
    }

    @Override
    public void decrementSecondsRemaining() {
	secondsRemaining--;
    }

    @Override
    public int getDuration() {
	return duration;
    }

    @Override
    public int getSecondsRemaining() {
	return secondsRemaining;
    }

    @Override
    public String getWatchDate() {
	return watchDate;
    }

    @Override
    public boolean isPlaying() {
	return playing;
    }

    @Override
    public boolean isRemovable() {
	return removable;
    }

    @Override
    public boolean isSecondsRemainingNegative() {
	return secondsRemaining < 0;
    }

    @Override
    public void play() {
	playing = true;
    }

    @Override
    public void setRemovable(boolean removable) {
	this.removable = removable;
    }

    /**
     * Makes the secondsRemaining become the duration. This should only be called when the schedule starts playing.
     */
    @Override
    public void switchSecondsRemaining() {
	secondsRemaining = duration;
    }

}
