package com.noobs2d.jumbocinema.youtube;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

import com.noobs2d.jumbocinema.MySQLDatabase;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.extensions.SFSExtension;

/**
 * Standard schedule operations to avoid code redundancy.
 * 
 * @author Julious Cious Igmen <jcigmen@gmail.com>
 */
public class YouTubeUtils {

    /**
     * Just getting a datetime from a result set ends in a string with a suffix of .0, so we
     * dedicate a quick method to get a well formatted DATETIME string
     * @param resultSet
     * @return MM-DD-YYYY HH:MM:SS
     * @throws SQLException 
     */
    public static String getFormattedDateTimeFromResultSet(ResultSet resultSet, String columnLabel) throws SQLException {
	Date date = resultSet.getDate(columnLabel);
	Time time = resultSet.getTime(columnLabel);
	return date.toString() + " " + time.toString();
    }

    public static void putDetailsFromScheduleToSFSObject(ISFSObject returnParams, int schedulesCount) throws SQLException, ClassNotFoundException {
	for (int i = 0; i < schedulesCount; i++) {
	    MySQLDatabase.connect();

	    String videoID = returnParams.getUtfString("VIDEO_ID[" + i + "]");
	    String watchDate = returnParams.getUtfString("WATCH_DATE[" + i + "]");

	    String query = "CALL youTubeGetSchedulesUsers('" + videoID + "', '" + watchDate + "');";
	    ResultSet resultSet = MySQLDatabase.executeQuery(query);
	    List<String> ids = new ArrayList<String>();
	    List<String> names = new ArrayList<String>();
	    int subIndex = 0;
	    while (resultSet.next()) {
		String id = resultSet.getString("user_id");
		String firstName = resultSet.getString("first_name");
		String lastName = resultSet.getString("last_name");

		ids.add(id);
		names.add(firstName + " " + lastName);

		subIndex++;
	    }
	    returnParams.putUtfStringArray("USER_IDS[" + i + "]", ids);
	    returnParams.putUtfStringArray("NAMES[" + i + "]", names);
	    returnParams.putInt("USER_COUNT[" + i + "]", subIndex);

	    MySQLDatabase.disconnect();
	}
    }

    /**
     * Accepts one of the following schedule query requests and gets the standard required data from the results:
     * 
     * CALL youTubeGetSchedules();
     * CALL youTubeGetSchedulesByUserID(userID);
     * 
     * The appropriate string query must be provided. Of course, an error in these strings will cause great errors.
     * 
     * @param query One of the enlisted queries
     * @return schedulesCount Number of schedules fetched
     * @throws ClassNotFoundException 
     * @throws SQLException 
     */
    public static int putStandardScheduleDataToSFSObject(SFSExtension parentExtension, ISFSObject returnParams, String query) throws SQLException, ClassNotFoundException {
	MySQLDatabase.connect();

	ResultSet resultSet = MySQLDatabase.executeQuery(query);

	int index = 0;
	while (resultSet.next()) {
	    String watchDate = getFormattedDateTimeFromResultSet(resultSet, "watch_date");
	    boolean playing = resultSet.getBoolean("playing");
	    int secondsRemaining = resultSet.getInt("seconds_before_showtime");
	    int duration = resultSet.getInt("duration");

	    returnParams.putUtfString("VIDEO_ID[" + index + "]", resultSet.getString("video_id"));
	    returnParams.putInt("REMAINING[" + index + "]", secondsRemaining);
	    returnParams.putBool("PLAYING[" + index + "]", playing);
	    returnParams.putUtfString("WATCH_DATE[" + index + "]", watchDate);
	    returnParams.putUtfString("ROOM_NAME[" + index + "]", resultSet.getString("ROOM_NAME"));

	    index++;
	}
	returnParams.putInt("ROW_COUNT", index);

	MySQLDatabase.disconnect();

	return index;
    }
}
