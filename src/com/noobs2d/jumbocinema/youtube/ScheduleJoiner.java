package com.noobs2d.jumbocinema.youtube;

import java.sql.SQLException;

import com.noobs2d.jumbocinema.ExtensionRequestHandler;
import com.noobs2d.jumbocinema.MySQLDatabase;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;

public class ScheduleJoiner extends ExtensionRequestHandler {

    public static final String TAG = "YOUTUBE_SCHEDULE_JOIN";

    @Override
    public void handleClientRequest(User sender, ISFSObject params) {
	final ISFSObject returnParams = SFSObject.newInstance();
	final String userID = params.getUtfString("USER_ID");
	final String videoID = params.getUtfString("VIDEO_ID");
	final String watchDate = params.getUtfString("WATCH_DATE");

	try {
	    MySQLDatabase.connect();

	    String query = "CALL youTubeJoinExistingScheduleByFullDetails('" + userID + "', '" + videoID + "', '" + watchDate + "');";
	    MySQLDatabase.executeQuery(query);

	    MySQLDatabase.disconnect();
	} catch (SQLException e) {
	    onExceptionEvent(getClass().getName(), e, returnParams);
	} catch (ClassNotFoundException e) {
	    onExceptionEvent(getClass().getName(), e, returnParams);
	}
	send(TAG, returnParams, sender);
    }

}
