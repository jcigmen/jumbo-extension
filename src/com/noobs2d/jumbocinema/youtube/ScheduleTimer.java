package com.noobs2d.jumbocinema.youtube;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import com.smartfoxserver.v2.SmartFoxServer;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.extensions.SFSExtension;

public class ScheduleTimer {

    private class TaskRunner implements Runnable {

	@Override
	public void run() {
	    checkRemoveableItems();
	    sendTime();
	}

	private void checkRemoveableItems() {
	    try {
		for (int i = 0; i < schedules.size(); i++)
		    if (schedules.get(i).isRemovable())
			schedules.remove(i);
	    } catch (Exception e) {
		parentExtension.trace("Error removing instance from collection: " + e.getMessage());
	    }
	}
    }

    private SFSExtension parentExtension;
    private ArrayList<ISchedule> schedules;

    public ScheduleTimer(SFSExtension parentExtension) {
	this.parentExtension = parentExtension;
	schedules = new ArrayList<ISchedule>();
	SmartFoxServer smartFoxServer = SmartFoxServer.getInstance();
	smartFoxServer.getTaskScheduler().scheduleAtFixedRate(new TaskRunner(), 0, 1, TimeUnit.SECONDS);
    }

    public void addSchedule(ISchedule scheduleToAdd) {
	try {
	    boolean notInCollectionYet = true;
	    for (int i = 0; i < schedules.size(); i++)
		if (schedules.get(i).getWatchDate() == scheduleToAdd.getWatchDate()) {
		    notInCollectionYet = false;
		    i = schedules.size(); //break this loop
		}

	    parentExtension.trace("Adding " + scheduleToAdd.getWatchDate() + " to the thread...");
	    if (notInCollectionYet) {
		schedules.add(scheduleToAdd);
		parentExtension.trace("Added " + scheduleToAdd.getWatchDate() + " to the thread!");

	    }
	} catch (Exception e) {
	    parentExtension.trace("Adding schedule on timer error: " + e.getMessage());
	}
    }

    private void sendTime() {
	try {
	    for (int i = 0; i < schedules.size(); i++) {
		schedules.get(i).decrementSecondsRemaining();

		boolean swapScheduleTimers = false;
		boolean removeItem = false;

		if (schedules.get(i).isSecondsRemainingNegative())
		    if (schedules.get(i).isPlaying())
			removeItem = true;
		    else {
			schedules.get(i).play();
			swapScheduleTimers = true;
		    }
		ISFSObject returnData = SFSObject.newInstance();
		returnData.putInt("REMAINING", schedules.get(i).getSecondsRemaining());
		returnData.putBool("PLAYING", schedules.get(i).isPlaying());
		returnData.putBool("ENDED", removeItem);

		parentExtension.send(schedules.get(i).getWatchDate(), returnData, parentExtension.getParentZone().getUserManager().getAllUsers());

		if (swapScheduleTimers)
		    schedules.get(i).switchSecondsRemaining();

		schedules.get(i).setRemovable(removeItem);
	    }
	} catch (Exception e) {
	    parentExtension.trace("Timer thread error: " + e.getMessage());
	}
    }

}
