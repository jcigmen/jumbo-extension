package com.noobs2d.jumbocinema.youtube;

public interface ISchedule {

    public void decrementSecondsRemaining();

    public int getDuration();

    public int getSecondsRemaining();

    public String getWatchDate();

    public boolean isPlaying();

    public boolean isRemovable();

    public boolean isSecondsRemainingNegative();

    public void play();

    public void setRemovable(boolean removable);

    /**
     * Makes the secondsRemaining become the duration. This should only be called when the schedule starts playing.
     */
    public void switchSecondsRemaining();
}
