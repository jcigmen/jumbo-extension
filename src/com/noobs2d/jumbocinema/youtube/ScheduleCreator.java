package com.noobs2d.jumbocinema.youtube;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.noobs2d.jumbocinema.ExtensionRequestHandler;
import com.noobs2d.jumbocinema.Main;
import com.noobs2d.jumbocinema.MainZoneExtension;
import com.noobs2d.jumbocinema.MySQLDatabase;
import com.smartfoxserver.v2.SmartFoxServer;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;

/**
 * Creates a YouTube watch schedule.
 * 
 * Required data from the received ISFSObject:
 * 
 * USER_ID (UtfString)
 * VIDEO_ID (UtfString)
 * SECONDS_DIFF (int)
 * DURATION (int)
 * 
 * @author MrUseL3tter
 */
public class ScheduleCreator extends ExtensionRequestHandler {

    public static final String TAG = "YOUTUBE_CREATE_SCHEDULE";

    @Override
    public void handleClientRequest(User sender, ISFSObject params) {
	final ISFSObject returnParams = SFSObject.newInstance();
	final String userID = params.getUtfString("USER_ID");
	final String videoID = params.getUtfString("VIDEO_ID");
	final int secondsDifference = params.getInt("SECONDS_DIFF");
	final int duration = params.getInt("DURATION");

	try {
	    MySQLDatabase.connect();

	    String query = "CALL youTubeGetSchedule('" + userID + "', '" + videoID + "', '" + secondsDifference + "');";
	    ResultSet resultSet = MySQLDatabase.executeQuery(query);

	    // if the result set has a next(), then the query returned a row, thus that schedule already exists
	    boolean alreadyExists = resultSet.next();
	    if (alreadyExists) {
		query = "CALL youTubeJoinExistingSchedule('" + userID + "', '" + videoID + "', '" + secondsDifference + "');";
		MySQLDatabase.executeQuery(query);

		returnParams.putInt("FLAGS", Main.SUCCESS);
		returnParams.putUtfString("FLAG_FEED", "Successfully joined an existing schedule!");
	    } else {
		// check if the user has any pending schedules. if there is one, he cant make a new one yet
		boolean doesntHaveScheduleYet = checkIfHasScheduleAlready(userID);
		if (doesntHaveScheduleYet) {
		    // create a new schedule!
		    query = "CALL youTubeCreateSchedule('" + userID + "', '" + videoID + "', " + secondsDifference + ", " + duration + ");";
		    resultSet = MySQLDatabase.executeQuery(query);

		    String watchDate = "";
		    int difference = 0;
		    while (resultSet.next()) {
			watchDate = YouTubeUtils.getFormattedDateTimeFromResultSet(resultSet, "WATCH_DATE");
			difference = resultSet.getInt("REMAINING");
			returnParams.putUtfString("ROOM_NAME", resultSet.getString("ROOM_NAME"));
			returnParams.putUtfString("WATCH_DATE", watchDate);
			returnParams.putInt("REMAINING", difference);
		    }

		    startScheduleTimer(difference, duration, watchDate);

		    returnParams.putInt("FLAGS", Main.SUCCESS);
		    returnParams.putUtfString("FLAG_FEED", "Successfully created a new schedule!");
		} else {
		    returnParams.putInt("FLAGS", Main.FAILED);
		    returnParams.putUtfString("FLAG_FEED", "You still have an existing schedule. Watch it first before you can make a new one!");
		}
	    }

	    MySQLDatabase.disconnect();
	} catch (SQLException e) {
	    onExceptionEvent(getClass().getName(), e, returnParams);
	} catch (ClassNotFoundException e) {
	    onExceptionEvent(getClass().getName(), e, returnParams);
	}
	send(TAG, returnParams, sender);
    }

    private boolean checkIfHasScheduleAlready(String userID) throws SQLException, ClassNotFoundException {
	boolean hasScheduleAlready = true;
	String query = "CALL youTubeGetSchedulesByUserID('" + userID + "');";
	ResultSet resultSet = MySQLDatabase.executeQuery(query);
	hasScheduleAlready = resultSet.next();

	query = "CALL youTubeGetSchedulesPlayingByUserID('" + userID + "');";
	resultSet = MySQLDatabase.executeQuery(query);
	hasScheduleAlready |= resultSet.next();

	return !hasScheduleAlready;
    }

    private void startScheduleTimer(int secondsRemaining, int duration, String watchDate) {
	MainZoneExtension zoneExtension = (MainZoneExtension) SmartFoxServer.getInstance().getZoneManager().getZoneByName("JC").getExtension();
	zoneExtension.getTimer().addSchedule(new RawSchedule(duration, false, secondsRemaining, watchDate));
    }

    @Override
    protected void onExceptionEvent(String callingClass, Exception e, ISFSObject returnParams) {
	returnParams.putInt("FLAGS", Main.ERROR);
	returnParams.putUtfString("FLAG_FEED", "There was an error trying to make the schedule. Make sure you haven't joined this schedule or that this schedule isn't created yet.");
	trace("[" + callingClass + "] Error: " + e);
	e.printStackTrace();
    }
}
