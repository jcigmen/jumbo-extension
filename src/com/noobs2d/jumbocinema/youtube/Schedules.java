package com.noobs2d.jumbocinema.youtube;

import java.sql.SQLException;

import com.noobs2d.jumbocinema.ExtensionRequestHandler;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;

/**
 * Retrieves all the scheduled videos that haven't played yet, sorted with the newely created ones.
 * These are then put into the ISFSObject that is returned into the calling User.
 * 
 * "ROW_COUNT" (int) - The amount of rows yield by this request. This is used to reference the index
 * of the entries we've got so if we received 42 rows from the query, we can use 0-41 as indexes
 * for the next data:
 * 
 * "VIDEO_ID[X]" (UtfString) - the id of the video. X is the index from the row. (i.e., VIDEO_ID[12] is the 13th
 * row in the query's result
 * 
 * "NAMES[X]" (UtfStringArray) - the names of the reservees, ordered alphabetically by first name
 * 
 * "USER_IDS[X]" - (UtfStringArray) - user IDs of the reservees
 * 
 * "USER_COUNT[X]" (int) - quantity of users that are going to watch this schedule. also the length of USER_IDS
 * 
 * "REMAINING[X]" (int) - the amount of seconds before the schedule is desired to play
 * 
 * "ROOM_NAME[X]" (UtfString)
 * 
 * back in the front end, the programmer will loop through the ISFSObject, using the ROW_COUNT as the 
 * counter flag, telling his loop to end.
 * 
 * @author Julious Cious Igmen <jcigmen@gmail.com>
 */
public class Schedules extends ExtensionRequestHandler {

    public static final String TAG = "YOUTUBE_SCHEDULES";

    @Override
    public void handleClientRequest(User sender, ISFSObject params) {
	final ISFSObject returnParams = SFSObject.newInstance();

	try {
	    String query = "CALL youTubeGetSchedules();";
	    int schedulesCount = YouTubeUtils.putStandardScheduleDataToSFSObject(getParentExtension(), returnParams, query);
	    if (schedulesCount > 0)
		YouTubeUtils.putDetailsFromScheduleToSFSObject(returnParams, schedulesCount);
	} catch (SQLException e) {
	    onExceptionEvent("BlockRemoveRequestHandler", e, returnParams);
	} catch (ClassNotFoundException e) {
	    onExceptionEvent("BlockRemoveRequestHandler", e, returnParams);
	}
	send(TAG, returnParams, sender);
    }
}
