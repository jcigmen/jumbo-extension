package com.noobs2d.jumbocinema.youtube;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import com.noobs2d.jumbocinema.ExtensionRequestHandler;
import com.noobs2d.jumbocinema.Main;
import com.smartfoxserver.v2.SmartFoxServer;
import com.smartfoxserver.v2.api.CreateRoomSettings;
import com.smartfoxserver.v2.api.CreateRoomSettings.RoomExtensionSettings;
import com.smartfoxserver.v2.entities.SFSRoomRemoveMode;
import com.smartfoxserver.v2.entities.SFSRoomSettings;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.Zone;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.entities.variables.RoomVariable;
import com.smartfoxserver.v2.entities.variables.SFSRoomVariable;
import com.smartfoxserver.v2.exceptions.SFSCreateRoomException;

/**
 * Just creates a new SFS room for a YouTube schedule.
 * This room will be removed once it becomes empty.
 */
public class RoomCreator extends ExtensionRequestHandler {

    public static final String TAG = "ROOM_CREATE";

    @Override
    public void handleClientRequest(User sender, ISFSObject params) {
	final ISFSObject returnParams = SFSObject.newInstance();
	String roomName = params.getUtfString("ROOM_NAME");
	if (roomName.length() > 15)
	    roomName = roomName.substring(0, 14);

	Zone zone = SmartFoxServer.getInstance().getZoneManager().getZoneByName("JC");
	List<RoomVariable> variables = getRoomVariables();

	RoomExtensionSettings roomExtensionSettings = new RoomExtensionSettings("jumbocinema", "com.noobs2d.jumbocinema.Main");
	roomExtensionSettings.setPropertiesFile("config.properties");

	Set<SFSRoomSettings> roomSettings = new TreeSet<SFSRoomSettings>();
	roomSettings.add(SFSRoomSettings.PASSWORD_STATE_CHANGE);
	roomSettings.add(SFSRoomSettings.PUBLIC_MESSAGES);
	roomSettings.add(SFSRoomSettings.USER_ENTER_EVENT);
	roomSettings.add(SFSRoomSettings.USER_EXIT_EVENT);
	roomSettings.add(SFSRoomSettings.USER_COUNT_CHANGE_EVENT);
	roomSettings.add(SFSRoomSettings.USER_VARIABLES_UPDATE_EVENT);

	CreateRoomSettings settings = new CreateRoomSettings();

	settings.setAutoRemoveMode(SFSRoomRemoveMode.WHEN_EMPTY);
	settings.setExtension(roomExtensionSettings);
	settings.setRoomSettings(roomSettings);
	settings.setUseWordsFilter(true);
	settings.setRoomVariables(variables);
	settings.setMaxUsers(24);
	settings.setMaxVariablesAllowed(100);
	settings.setName(roomName);
	settings.setDynamic(true);

	try {
	    getApi().createRoom(zone, settings, null, false, sender.getLastJoinedRoom());
	    returnParams.putInt("FLAGS", Main.SUCCESS);
	    returnParams.putUtfString("FLAG_FEED", "Successfully created a new SFS Room named " + roomName);
	} catch (SFSCreateRoomException e) {
	    onExceptionEvent(getClass().getName(), e, returnParams);
	    e.printStackTrace();
	}
	returnParams.putUtfString("ROOM_NAME", roomName);
	send(TAG, returnParams, sender);
    }

    private List<RoomVariable> getRoomVariables() {
	List<RoomVariable> variables = new ArrayList<RoomVariable>();
	for (int i = 1; i <= 6; i++) {
	    int max = i == 6 ? 6 : 8;
	    for (int j = 1; j <= max; j++) {
		String roomName = "row_" + i + "_seat_" + j;
		SFSRoomVariable variable = new SFSRoomVariable(roomName, "null");
		variables.add(variable);
	    }
	}
	return variables;
    }

}
