package com.noobs2d.jumbocinema;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

/**
 * When a user logs in, if they haven't logged in before, 
 * they will be added to the database, otherwise we'll 
 * just log their login time.
 * 
 * @author michael
 *
 */
public class LogoutReqHandler extends BaseClientRequestHandler{

	@Override
	public void handleClientRequest(User sender, ISFSObject params) 
	{		
		String sessionId = new Integer(params.getInt("sessionId")).toString();		
		
		ISFSObject resObj = logout(sessionId, sender);
	         
        // Send it back
        send("logoutRequest", resObj, sender);		
	}
	
	/** 
	 * Static so that other we can access this method programmaticaly 
	 * as well as from the extension listener.
	 * 
	 * @param sessionId
	 * @param sender
	 * @return
	 */
	public static ISFSObject logout(String sessionId, User sender)
	{
		int ret = -1;
		String dbUrl = "jdbc:mysql://" + DbConfig.getDbIp() + "/" + DbConfig.getDb() + "?user=" + DbConfig.getUser() + "&password=" + DbConfig.getPassword();/*"jdbc:mysql://localhost/jumbotest?user=michael&password=thuglife";*/
		String query = "UPDATE " + DbConfig.getDb() + ".log SET endTime = NOW() WHERE idlog = " + sessionId;
		
		try 
		{
			Class.forName("com.mysql.jdbc.Driver");	
			Connection con = DriverManager.getConnection(dbUrl);
			Statement stmt = con.createStatement();
			ret = stmt.executeUpdate(query);			
	
			con.close();	
		
		} catch (ClassNotFoundException e) {
			ret = -1;
			e.printStackTrace();
		} catch (SQLException e) {
			ret = -1;
			e.printStackTrace();
		}
		
		ISFSObject resObj = SFSObject.newInstance(); 
        resObj.putInt("logoutReturn", ret);
        resObj.putUtfString("query", query);
        
        return resObj;
	}

}
