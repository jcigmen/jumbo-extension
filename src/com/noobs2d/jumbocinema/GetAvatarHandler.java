package com.noobs2d.jumbocinema;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

/**
 * When a user logs in, if they haven't logged in before, they will be added to the database,
 * otherwise we'll just log their login time.
 * 
 * @author michael
 */
public class GetAvatarHandler extends BaseClientRequestHandler {

    @Override
    public void handleClientRequest(User sender, ISFSObject params) {
	long userId = params.getLong("userId");
	int avatarId = -1;
	String gender = "", hairFileName = "", outfitFileName = "", skinFileName = "", hatFileName = "";

	String dbUrl = "jdbc:mysql://" + DbConfig.getDbIp() + "/" + DbConfig.getDb() + "?user=" + DbConfig.getUser() + "&password=" + DbConfig.getPassword();/*
																			      * "jdbc:mysql://localhost/jumbotest?user=michael&password=thuglife"
																			      * ;
																			      */

	String avatarIdSelect = "SELECT avatarId FROM " + DbConfig.getDb() + ".users WHERE idusers = '" + userId + "'";
	try {
	    Class.forName("com.mysql.jdbc.Driver");
	    Connection connection = DriverManager.getConnection(dbUrl);
	    Statement statement = connection.createStatement();

	    // get the hair id
	    statement.executeQuery(avatarIdSelect);
	    ResultSet resultSet = statement.getResultSet();
	    if (resultSet != null && resultSet.next())
		avatarId = resultSet.getInt(1);

	    // get the outfit id
	    statement.executeQuery("CALL selectAvatarInfoByID('" + avatarId + "');");

	    resultSet = statement.getResultSet();
	    if (resultSet != null && resultSet.next()) {
		gender = resultSet.getString("gender");
		hairFileName = resultSet.getString("hairFileName");
		outfitFileName = resultSet.getString("outfitFileName");
		skinFileName = resultSet.getString("skinFileName");
		hatFileName = resultSet.getString("hatFileName");
	    }

	    connection.close();

	} catch (ClassNotFoundException e) {
	    trace("[GetAvatarHandler] " + e);
	    e.printStackTrace();
	} catch (SQLException e) {
	    trace("[GetAvatarHandler] " + e);
	    e.printStackTrace();
	}

	ISFSObject resObj = SFSObject.newInstance();
	resObj.putUtfString("gender", gender);
	resObj.putUtfString("hair", hairFileName);
	resObj.putUtfString("outfit", outfitFileName);
	resObj.putUtfString("skin", skinFileName);
	resObj.putUtfString("hat", hatFileName);

	// Send it back
	send("getAvatar", resObj, sender);
    }

}
