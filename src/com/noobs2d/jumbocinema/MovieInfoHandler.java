package com.noobs2d.jumbocinema;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import com.noobs2d.jumbocinema.classes.Movie;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;
import com.smartfoxserver.v2.extensions.BaseSFSExtension;

/**
 * This class retrieves the information for the movie in the specified theater. This includes
 * calculating the header position of the player tracker, ratings, duration etc.
 * 
 * @author Michael
 */
public class MovieInfoHandler extends BaseClientRequestHandler {

    public static final String TAG = "STAFF_PICK_INFO_BY_SHOWTIME_ID";

    private DatabaseConnect _dc;

    @Override
    public void handleClientRequest(User sender, ISFSObject params) {
	// Loads the config.properties file located in the folder that this extension sits in
	Properties props = ((BaseSFSExtension) getParentExtension()).getConfigProperties();
	long lagSeconds = new Long(props.getProperty("lagSeconds")).longValue();

	String theater = params.getUtfString("theater");
	theater = theater.substring(0, theater.length() - 2);
	int theaterNo = Integer.parseInt(theater.substring(7));

	trace("[MovieInfoHandler] Getting movie info on " + theater);

	_dc = new DatabaseConnect();
	Movie movie = _dc.getMovie(theaterNo);

	// Create a response object
	ISFSObject resObj = SFSObject.newInstance();
	resObj.putUtfString("theater", theater);
	resObj.putUtfString("movieTitle", movie.getMovieTitle());
	resObj.putUtfString("status", movie.getStatus());

	resObj.putDouble("rating", movie.getRatingObject().getScalePos());

	resObj.putInt("movieId", movie.getMovieId());
	resObj.putInt("showtimeId", movie.getShowtimeId());

	resObj.putLong("headerPos", movie.getTimeToShowtime());
	resObj.putLong("movieTime", movie.getShowTime());
	resObj.putLong("lagSeconds", lagSeconds);
	resObj.putLong("movieLength", movie.getMovieLength());
	resObj.putUtfString("movieLink", movie.getLink());

	String dbUrl = "jdbc:mysql://" + DbConfig.getDbIp() + "/" + DbConfig.getDb() + "?user=" + DbConfig.getUser() + "&password=" + DbConfig.getPassword();
	try {
	    Class.forName("com.mysql.jdbc.Driver");
	    Connection con = DriverManager.getConnection(dbUrl);

	    String query = "CALL selectSecondsElapsedByShowingID(" + movie.getShowtimeId() + ");";
	    Statement stmt = con.createStatement();
	    ResultSet rs = stmt.executeQuery(query);
	    if (rs != null && rs.next())
		resObj.putLong("secondsElapsed", rs.getLong(1));
	} catch (SQLException e) {
	    trace("[MovieInfoHandler] " + e);
	    e.printStackTrace();
	} catch (ClassNotFoundException e) {
	    trace("[MovieInfoHandler]  " + e);
	    e.printStackTrace();
	}

	// Send it back
	send("getMovieInfo", resObj, sender);
    }

}
