package com.noobs2d.jumbocinema;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.smartfoxserver.v2.core.ISFSEvent;
import com.smartfoxserver.v2.core.SFSEventParam;
import com.smartfoxserver.v2.core.SFSEventType;
import com.smartfoxserver.v2.entities.SFSRoom;
import com.smartfoxserver.v2.entities.SFSUser;
import com.smartfoxserver.v2.entities.variables.RoomVariable;
import com.smartfoxserver.v2.entities.variables.SFSRoomVariable;
import com.smartfoxserver.v2.exceptions.SFSException;
import com.smartfoxserver.v2.extensions.BaseServerEventHandler;

/**
 * Handles removing the user's avatar from a theater when they leave
 * or disconnect from a room.
 * 
 * Also logs the user's logout time if they disconnect without 
 * properly logging out.
 * 
 * @author Michael
 *
 */
public class UserDisconnectHandler extends BaseServerEventHandler {

    @Override
    public void handleServerEvent(ISFSEvent event) throws SFSException {
	SFSEventType evtType = event.getType();
	List<SFSRoom> roomList = new ArrayList<SFSRoom>(); // init

	// Get the user info
	SFSUser user = (SFSUser) event.getParameter(SFSEventParam.USER);
	int clientUserId = user.getId();

	// Checks to see if the user disconnected or left the room, which will 
	// affect how we retrieve the room object
	if (evtType == SFSEventType.USER_LEAVE_ROOM) {
	    SFSRoom room = (SFSRoom) event.getParameter(SFSEventParam.ROOM);
	    roomList.add(room);
	} else if (evtType == SFSEventType.USER_DISCONNECT) {
	    // Log the user out
	    int dbUserId = user.getVariable("sessionId").getIntValue();
	    LogoutReqHandler.logout(Integer.toString(dbUserId), null);

	    // Grab their last room
	    roomList = (List<SFSRoom>) event.getParameter(SFSEventParam.JOINED_ROOMS);
	}

	Iterator<SFSRoom> it = roomList.iterator();

	// Iterate through the rooms joined, looking for the theater the user was in
	while (it.hasNext()) {
	    // Get room
	    SFSRoom room = it.next();
	    String roomName = room.getName();

	    // Iterate through room variables to find which variable corresponds to userId
	    List<RoomVariable> rvlist = room.getVariables();
	    Iterator<RoomVariable> rvit = rvlist.iterator();

	    while (rvit.hasNext()) {
		SFSRoomVariable rv = (SFSRoomVariable) rvit.next();
		String varName = rv.getName();
		String varVal = (String) rv.getValue();

		String[] splitArr = null;

		if (!varVal.equals("null")) {
		    // The room variable value will be fm_hair_5_userid_0
		    int varUserId = 0;
		    try {
			splitArr = varVal.split("_");
			varUserId = Integer.parseInt(splitArr[4]);
		    } catch (Exception e) {

		    }

		    // We found the value in the room variables that corresponds to the client  
		    if (clientUserId == varUserId) {

			// Clear the room variable by setting its value to null
			List<RoomVariable> list = new ArrayList<RoomVariable>();
			list.add(new SFSRoomVariable(varName, "null"));

			// Send the update back over. The update will be broadcast to all clients in the room
			getApi().setRoomVariables(null, room, list);
		    }
		}
	    }//endwhile				
	}//endif		
    }
}
